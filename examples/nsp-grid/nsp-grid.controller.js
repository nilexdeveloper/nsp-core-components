(function() {
    'use strict';

    angular
        .module('demo')
        .controller('NspGridController', NspGridController);

    /* @ngInject */
    function NspGridController($scope) {
        var vm = this;

        vm.grid = {
            gridId: 'testGrid',
            // searchableColumns: ["firstName"],
            gridOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: 'http://localhost:1800/CmdbPersonView/GetDataSource'
                            // data: {
                            //     param1: 'testera'
                            // }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                firstName: {
                                    type: "string",
                                    dbType: "string"
                                },
                                mail: {
                                    type: "string",
                                    dbType: "string"
                                }
                            }
                        }
                    },
                    pageSize: 5
                    // sort: [{
                    //     field:"firstName",
                    //     dir:"desc",
                    //     fixed: true
                    // }],
                    // filter: [{
                    //     field: 'firstName',
                    //     operator: 'contains',
                    //     value: 'Dejan',
                    //     fixed: true
                    // }]
                },
                columns: [{
                    field: 'firstName',
                    title: 'Name',
                    hidden: false,
                    searchable: true,
                    encoded: false
                }, {
                    field: 'mail',
                    title: 'Email',
                    hidden: false,
                    searchable: true,
                    encoded: false
                }]
            },
            searchOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: 'http://localhost:1800/CmdbPersonView/GetAutoCompleteDataSource'
                        }
                    }
                }
            },
            onReset: function(data) {
                console.log('reset');
            }
        };
    }

})();