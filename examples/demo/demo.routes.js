(function () {
    'use strict';

    angular.module('demo').config(config);

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('/', '/app/home');
        $urlRouterProvider.when('', '/app/home');

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('app.home', {
                url: '/home',
                templateUrl: "examples/home/home.html"
            })
            .state('app.entity-properties', {
                url: '/entity-properties',
                templateUrl: "examples/entity-properties/entity-properties.html",
                controller: "EntityPropertiesController",
                controllerAs: 'vm'
            }).state('app.nsp-grid', {
                url: '/nsp-grid',
                templateUrl: "examples/nsp-grid/nsp-grid.html",
                controller: "NspGridController",
                controllerAs: 'vm'
            });
    }

})();
