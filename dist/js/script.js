
/****************************************
    FILE: src/nsp-core.module.js
****************************************/

/**
 * NSP Core
 */
angular.module('nspCore', []);

/****************************************
    FILE: src/nsp-core.service.js
****************************************/

(function() {
    'use strict';

    angular
        .module('nspCore')
        .factory('nspCoreService', nspCoreService);

    nspCoreService.$inject = ['$http', '$sce'];

    /* @ngInject */
    function nspCoreService($http, $sce) {

        var factory = {
            propertyExists: propertyExists,
            translate: translate,
            timeAgo: timeAgo,
            timeAgoLegacy: timeAgoLegacy,
            getBrowser: getBrowser,
            htmlEncode: htmlEncode,
            htmlDecode: htmlDecode,
            secondsToHoursAndMinutes: secondsToHoursAndMinutes,
            getParameterByName: getParameterByName
        };

        var tempDocument = null;

        return factory;

        function propertyExists(obj, key) {
            return key.split(".").every(function(x) {
                if (typeof obj != "object" || obj === null || !(x in obj))
                    return false;
                obj = obj[x];
                return true;
            });
        }

        function translate(text, key) {
            var result = key;
            if (NSP && NSP.Translate && NSP.Translate.t(key)) {
                result = NSP.Translate.t(key);
            }
            return result;
        }

        /**
         * Moment.js time ago formatter
         * @param  {string} value Date value in ISO standard
         * @return {string} Time ago formatted date
         */
        function timeAgo(value) {
            if (moment && value) {
                return moment(value).fromNow();
            }
        }

        /**
         * Legacy time ago formatter
         * @param  {string} dateString Date value in iso standard
         * @param  {string} pattern    Date format
         * @return {string} Time ago formatted date
         */
        function timeAgoLegacy(dateString, pattern) {
            if (dateString === null) {
                return '';
            } else {

                var date = new Date(dateString);

                if (date < new Date()) {
                    var seconds = Math.floor((new Date() - date) / 1000);
                    var interval = Math.floor(seconds / 31536000);
                    interval = Math.floor(seconds / 2592000);
                    interval = Math.floor(seconds / 86400);
                    interval = Math.floor(seconds / 3600);

                    interval = Math.floor(seconds / 60);

                    if (seconds <= 59) {
                        return translate("Just now", "JustNow");
                    } else if (seconds > 59 && seconds <= 119) {
                        return interval + " " + factory.translate("minute ago", "MinuteAgo");
                    } else if (seconds > 119 && seconds <= 3599) {
                        return interval + " " + factory.translate("minutes ago", "MinutesAgo");
                    } else {
                        return kendo.toString(date, pattern);
                    }
                } else {
                    return kendo.toString(date, pattern);
                }
            }
        }

        function getBrowser() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                if (tem !== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) !== null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        }

        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value) {
            return $sce.trustAsHtml($('<div/>').html(value).text());
        }

        function secondsToHoursAndMinutes(seconds) {
            var result = "",
                hours = seconds ? Math.floor(seconds / 3600) : 0,
                minutes = seconds ? Math.floor((seconds - (hours * 3600)) / 60) : 0;
            if (hours.toString().length === 1) {
                hours = '0' + hours;
            }
            if (minutes.toString().length === 1) {
                minutes = '0' + minutes;
            }
            result = hours + ":" + minutes;
            return result;
        }

        function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

    }
})();

/****************************************
    FILE: src/nsp-core.templates.js
****************************************/

angular.module('nspCore').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('/comments/nsp-comment-attachment-list.html',
    "<ul class=\"mailing-form\"><li ng-repeat=\"attachment in items\" title=\"{{attachment.fileName}}\"><a href=\"#\" ng-click=\"downloadAttachment(attachment)\"><img ng-src=\"{{attachmentImageSrc(attachment)}}\" alt=\"{{attachment.fileName}}\"></a></li></ul>"
  );


  $templateCache.put('/comments/nsp-comment-preview.html',
    "<div kendo-window-content><div ng-bind-html=\"dialogData.message\"></div></div>"
  );


  $templateCache.put('/comments/nsp-comment-timer.html',
    "<!-- TODO: Localization --><div ng-form name=\"timerForm\"><label>{{t('Timer', 'Common.Timer')}}:</label><button ng-click=\"toggle($event)\" class=\"btn btn-default\">{{toggleBtnText}}</button> <input name=\"timerInput\" type=\"text\" placeholder=\"hh:mm\" ng-model=\"timerInput\" ng-pattern=\"timerPattern\" ng-change=\"inputChanged()\" ng-focus=\"timerInputFocus()\" ng-blur=\"timerInputBlur()\"></div>"
  );


  $templateCache.put('/comments/nsp-comments-form.html',
    "<div class=\"comments-form\"><form sf-schema=\"schema\" sf-form=\"form\" sf-model=\"model\" name=\"{{formId}}\" id=\"{{formId}}\" ng-submit=\"onCommentSubmit()\" autocomplete=\"false\" class=\"form-engine\"></form><div class=\"form-actions\"><div file-drop file-drop-options=\"settings.uploadAttachmentsOptions\" class=\"file-drop\" ng-model=\"newComment.attachments\" ng-if=\"settings.uploadAttachmentsOptions\"></div><button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('public')\" ng-class=\"{active: newComment.isPublic}\" title=\"{{t('Change to public comment', 'PublicReply')}}\" ng-if=\"settings.publicMark\"><i class=\"icon-users\"></i></button> <button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('private')\" ng-class=\"{active: newComment.isPrivate}\" title=\"{{t('Change to internal comment','InternalNote')}}\" ng-if=\"settings.privateMark\"><i class=\"icon-user\"></i></button> <button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('solution')\" ng-class=\"{active: newComment.isSolution}\" ng-if=\"settings.solutionMark\" title=\"{{t('Mark as a Solution', 'MarkAsASolution')}}\"><i class=\"icon-lamp\"></i></button> <button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('workaround')\" ng-class=\"{active: newComment.isWorkaround}\" ng-if=\"settings.workaroundMark\" title=\"{{t('Mark as a Workaround', 'MarkAsAWorkaround')}}\"><i class=\"icon-loop\"></i></button> <span class=\"separator\" ng-hide=\"!settings.privateMark && !settings.publicMark\">|</span> <button class=\"btn btn-default\" ng-click=\"onCommentSubmit()\">{{t('Save comment', 'Common.SaveComment')}}</button> <span class=\"separator\">|</span> <span class=\"comment-mark-info\" ng-if=\"newComment.isPublic\" ng-if=\"settings.publicMark\">{{t(\"Your comment will be sent as a public reply\", \"PublicReplyCommentDescription\")}} </span><span class=\"comment-mark-info\" ng-if=\"newComment.isPrivate\" ng-if=\"settings.privateMark\">{{t(\"Your comment will be sent as an internal note\", \"InternalReplyCommentDescription\")}} </span><span class=\"separator\">|</span> <span class=\"comment-mark-info\" ng-if=\"newComment.isSolution\" ng-if=\"settings.solutionMark\">{{t(\"Your comment will be marked as solution\", \"SolutionReplyCommentDescription\")}} </span><span class=\"comment-mark-info\" ng-if=\"newComment.isWorkaround\" ng-if=\"settings.workaroundMark\">{{t(\"Your comment will be marked as workaround\", \"WorkaroundReplyCommentDescription\")}}</span></div></div>"
  );


  $templateCache.put('/comments/nsp-comments-list.html',
    "<div class=\"comments-list\" ng-if=\"commentsList.length\"><div class=\"heading\"><div class=\"comment-filters\"><button class=\"btn btn-default comment-mark\" title=\"{{t('Show solution comments only', 'ShowSolutionCommentsOnly')}}\" ng-class=\"{active: activeMarkFilter === 'solution'}\" ng-click=\"markFilter('solution')\" ng-if=\"countComments('solution')\"><i class=\"icon-lamp\"></i> <span class=\"count\">{{countComments('solution')}}</span></button> <button class=\"btn btn-default comment-mark\" title=\"{{t('Show workaround comments only', 'ShowWorkaroundCommentsOnly')}}\" ng-click=\"markFilter('workaround')\" ng-class=\"{active: activeMarkFilter === 'workaround'}\" ng-if=\"countComments('workaround')\"><i class=\"icon-loop\"></i> <span class=\"count\">{{countComments('workaround')}}</span></button> <button class=\"btn btn-default comment-mark\" title=\"{{t('Show attachment comments only', 'ShowAttachmentCommentsOnly')}}\" ng-click=\"markFilter('attachments')\" ng-class=\"{active: activeMarkFilter === 'attachments'}\" ng-if=\"countComments('attachments')\"><i class=\"icon-attach\"></i> <span class=\"count\">{{countComments('attachments')}}</span></button> <button class=\"btn btn-default comment-mark\" title=\"{{t('Comments', 'Common.Comments')}}\" ng-click=\"markFilter('all')\" ng-class=\"{active: activeMarkFilter === 'all'}\"><i class=\"icon-comment\"></i> <span class=\"count\">{{countComments('all')}}</span></button></div><div class=\"comments-search\"><input class=\"comments-search\" type=\"text\" ng-model=\"searchValue\" placeholder=\"{{t('Search comment', 'SearchComment')}}...\"> <button class=\"btn\"><i class=\"k-icon k-i-search\"></i></button></div><button class=\"btn collapse-all\" ng-click=\"collapseToggleAll()\"><i ng-class=\"{'icon-plus-squared': allCollapsed, 'icon-minus-squared': !allCollapsed}\"></i> {{allCollapsed ? t('Expand all', 'ExpandAll') : t('Collapse all', 'CollapseAll')}}</button></div><ul class=\"list\"><li ng-repeat=\"item in commentsList | filterComments:searchValue:activeMarkFilter\" ng-class=\"{'marked-as-public':item.isPublic, 'marked-as-private':item.isPrivate, 'marked-as-solution':item.isSolution, 'marked-as-workaround':item.isWorkaround, 'collapsed':item.collapsed}\"><div class=\"aside\"><img ng-src=\"{{item.avatar}}\" alt=\"User image\" ng-if=\"item.avatar\" class=\"avatar\"> <span class=\"collapser\" ng-click=\"collapseToggle(item.id)\" ng-class=\"{'f-nsp f-nsp-angle-down':!item.collapsed, 'f-nsp f-nsp-angle-right':item.collapsed}\"></span></div><div class=\"inner\"><div class=\"heading\"><div class=\"comment-actions\"><button class=\"btn\" ng-if=\"settings.sendMail\" ng-click=\"sendEmail(item)\" title=\"{{t('SendMail','Common.SendEmail')}}\"><i class=\"icon-mail\"></i></button> <button class=\"btn\" ng-if=\"settings.solutionMark\" ng-click=\"mapArticle(item)\" title=\"{{t('CreateArticle','Common.CreateArticle')}}\"><i class=\"f-nsp f-nsp-knowledge-base\"></i></button> <button class=\"btn btn-like\" ng-click=\"likeComment(item.id)\" ng-class=\"{'active': item.isLiked}\"><i class=\"f-nsp f-nsp-support\"></i> <span class=\"count\">{{item.likes}}</span></button> <button class=\"btn\" ng-if=\"settings.solutionMark && !item.isSolution && !item.isWorkaround\" ng-click=\"changeCommentMark(item.id, 'solution')\" title=\"{{t('Mark as a Solution', 'MarkAsASolution')}}\"><i class=\"icon-lamp\"></i></button> <button class=\"btn\" ng-if=\"settings.workaroundMark && !item.isSolution && !item.isWorkaround\" ng-click=\"changeCommentMark(item.id, 'workaround')\" title=\"{{t('Mark as a Workaround', 'MarkAsAWorkaround')}}\"><i class=\"icon-loop\"></i></button> <button class=\"btn\" ng-if=\"settings.solutionMark && item.isSolution\" ng-click=\"changeCommentMark(item.id, 'solution')\" title=\"{{t('Not a Solution', 'NotASolution')}}\"><i class=\"icon-block\"></i></button> <button class=\"btn\" ng-if=\"settings.workaroundMark && item.isWorkaround\" ng-click=\"changeCommentMark(item.id, 'workaround')\" title=\"{{t('Not a Workaround', 'NotAWorkaround')}}\"><i class=\"icon-block\"></i></button> <button class=\"btn\" ng-if=\"settings.publicMark && item.isPrivate\" ng-click=\"changeCommentMark(item.id, 'public')\" title=\"{{t('Change to public comment', 'PublicReply')}}\"><i class=\"icon-users\"></i></button> <button class=\"btn\" ng-if=\"settings.privateMark && item.isPublic\" ng-click=\"changeCommentMark(item.id, 'private')\" title=\"{{t('Change to internal comment','InternalNote')}}\"><i class=\"icon-user\"></i></button> <span class=\"date\">{{timeAgo(item.createdDate, 'g')}}</span></div><div class=\"author\"><span class=\"source-icon\" ng-class=\"item.sourceIcon\"></span> {{item.author}} <span class=\"time-spent\" ng-if=\"settings.timeSpent !== false\">({{t('Time spent', 'Common.TimeSpent')}}: {{formatTimeSpent(item.timeSpent)}})</span></div></div><div class=\"comment\" ng-bind-html=\"item.comment\" text-pane-popup-images></div><ul class=\"attachments\" ng-if=\"item.attachments\"><li ng-repeat=\"attachment in item.attachments\" title=\"{{attachment.fileName}}\"><a href=\"#\" ng-click=\"downloadAttachment(attachment)\"><img ng-src=\"{{attachmentImageSrc(attachment)}}\" alt=\"{{attachment.fileName}}\"></a></li></ul></div></li></ul></div>"
  );


  $templateCache.put('/comments/nsp-send-email-comment.html',
    "<div ng-controller=\"NspSendEmailCommentController\" kendo-window-content><form sf-schema=\"schema\" sf-form=\"form\" sf-model=\"model\" name=\"{{formId}}\" id=\"{{formId}}\" ng-submit=\"onSubmit()\" autocomplete=\"false\" class=\"form-engine add-edit-window\"></form><div nsp-comment-attachments-list comment-attachments-list-items=\"dialogData.comment.attachments\" ng-if=\"dialogData.comment.attachments\"></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-location-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view location-view\"><div class=\"map\" ng-class=\"{'no-location': !data.location}\"><div ng-if=\"!data.location\" class=\"no-location-info\"><p>{{t('No', 'Common.No')}}<br>{{t('Location', 'Common.Location')}}<br>{{t('Found', 'Common.Found')}}</p></div><div class=\"map-canvas\" id=\"map-canvas\"></div><div class=\"address-details\"><p class=\"list-title\"><b>{{t('Address', 'Common.Address')}}:</b></p><ul><li ng-if=\"data.address\">{{data.address}}</li><li ng-if=\"data.city\">{{data.city}}</li><li ng-if=\"data.zip\">{{data.zip}}</li><li ng-if=\"data.country\">{{data.country}}</li><li ng-if=\"data.countryCode\">{{data.countryCode}}</li></ul></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-product-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view product-view\"><div class=\"basic\"><p><b>{{data.name}}</b></p><div ng-bind-html=\"trustHtml(data.description) || '-'\"></div></div><div class=\"additional-info\"><div class=\"product-details\"><table><tr ng-if=\"data.ciTypeName\"><td>{{t('CI Type', 'Common.CIType')}}</td><td>{{data.ciTypeName}}</td></tr><tr ng-if=\"data.manufacturerName\"><td>{{t('Manufacturer', 'Common.Manufacturer')}}</td><td>{{data.manufacturerName}}</td></tr><tr ng-if=\"data.depreciationTypeName\"><td>{{t('Depreciation Model', 'Common.DepreciationModel')}}</td><td>{{data.depreciationTypeName}}</td></tr></table></div><div class=\"code\"><div ng-if=\"qrcode\" class=\"qr-code\"><span kendo-qrcode k-error-correction=\"'M'\" ng-model=\"qrcode\" k-size=\"90\"></span></div><div ng-if=\"data.codeString && data.codeType\" class=\"barcode\"><span kendo-barcode k-type=\"data.codeType\" ng-model=\"data.codeString\" k-height=\"60\" k-width=\"130\"></span></div></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-user-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view user-view\"><div class=\"info\"><div class=\"basic\"><p><b>{{data.firstName}} {{data.lastName}}</b></p><p><b><i>{{data.personTitle}}</i></b></p></div><div class=\"details\"><ul class=\"contact-details\"><li ng-if=\"data.mail\">{{data.mail}}</li><li ng-if=\"data.phone\">{{data.phone}}</li></ul><ul class=\"department-details\"><li ng-if=\"data.department\">{{data.department}}</li><li ng-if=\"data.jobTitle\">{{data.jobTitle}}</li><li ng-if=\"data.birthDate\">{{data.birthDate}}</li></ul></div></div><div class=\"map\" ng-class=\"{'no-location': !data.location}\"><div ng-if=\"!data.location\" class=\"no-location-info\"><p>{{t('No', 'Common.No')}}<br>{{t('Location', 'Common.Location')}}<br>{{t('Found', 'Common.Found')}}</p></div><div class=\"map-canvas\" id=\"map-canvas\"></div><div class=\"address-details\" ng-if=\"data.address || data.city || data.zip || data.country || data.countyCode\"><p class=\"list-title\"><b>{{t('Address', 'Common.Address')}}:</b></p><ul><li ng-if=\"data.address\">{{data.address}}</li><li ng-if=\"data.city\">{{data.city}}</li><li ng-if=\"data.zip\">{{data.zip}}</li><li ng-if=\"data.country\">{{data.country}}</li><li ng-if=\"data.countryCode\">{{data.countryCode}}</li></ul></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-vendor-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view vendor-view\"><div class=\"info\"><div class=\"basic\"><p><b>{{data.name}}</b></p><div ng-bind-html=\"trustHtml(data.description) || '-'\"></div></div><div class=\"details\"><p><b>{{t('Contact', 'Common.Contact')}}:</b></p><ul><li ng-if=\"data.contactPerson\"><b>{{data.contactPerson}}</b></li><li ng-if=\"data.phone\">{{data.phone}}</li><li ng-if=\"data.fax\">{{data.fax}}</li><li ng-if=\"data.mobile\">{{data.mobile}}</li><li ng-if=\"data.mail\">{{data.mail}}</li></ul></div></div><div class=\"map\" ng-class=\"{'no-location':!data.location}\"><div ng-if=\"!data.location\" class=\"no-location-info\"><p>{{t('No', 'Common.No')}}<br>{{t('Location', 'Common.Location')}}<br>{{t('Found', 'Common.Found')}}</p></div><div class=\"map-canvas\" id=\"map-canvas\"></div><div class=\"address-details\"><p class=\"list-title\"><b>{{t('Address', 'Common.Address')}}:</b></p><ul><li ng-if=\"data.address\">{{data.address}}</li><li ng-if=\"data.city\">{{data.city}}</li><li ng-if=\"data.zip\">{{data.zip}}</li><li ng-if=\"data.country\">{{data.country}}</li><li ng-if=\"data.countryCode\">{{data.countryCode}}</li></ul></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/check-box.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div class=\"value\"><div ng-if=\"options.value\" class=\"check-box-checked\"></div><div ng-if=\"!options.value\" class=\"check-box-unchecked\"></div><i class=\"f-nsp\" ng-class=\"{'f-nsp-checked': options.value, 'f-nsp-unchecked': !options.value}\"></i> <span class=\"option-label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/check-boxes.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><div ng-repeat=\"item in options.value\"><div ng-if=\"item.Value\" class=\"check-box-checked\"></div><div ng-if=\"!item.Value\" class=\"check-box-unchecked\"></div><i class=\"f-nsp\" ng-class=\"{'f-nsp-checked': item.Value, 'f-nsp-unchecked': !item.Value}\"></i> <span>{{t(item.Title, item.TitleKey)}}</span></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/image.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\" nsp-core-text-pane-popup-images><img ng-if=\"options.value\" ng-src=\"{{options.value}}\" alt=\"{{options.label}}\"> <span ng-if=\"!options.value\">-</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/popup.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><span ng-click=\"openPopup()\" ng-class=\"{'link': options.id}\">{{options.value ? options.value : '-'}}</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/radio-button.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><div ng-repeat=\"item in options.value track by $index\"><div ng-if=\"item.Value\" class=\"radio-button-checked\"></div><div ng-if=\"!item.Value\" class=\"radio-button-unchecked\"></div><i class=\"f-nsp\" ng-class=\"{'f-nsp-radio': item.Value, 'f-nsp-circle-o': !item.Value}\"></i> <span>{{t(item.Title, item.TitleKey)}}</span></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/text.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><span>{{options.value ? options.value : '-'}}</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/textarea.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\" ng-bind-html=\"trustHtml(options.value) || '-'\" nsp-core-text-pane-popup-images></div></div>"
  );


  $templateCache.put('/kendo-window/delete-window.html',
    "<div class=\"dynamic-grid-delete-window\" delete-window><p>{{message}}</p><button class=\"btn btn-primary\" ng-click=\"delete()\">{{confirmButtonText}}</button> <button class=\"btn btn-primary\" ng-click=\"cancel()\">{{cancelButtonText}}</button></div>"
  );


  $templateCache.put('/notification/notification.html',
    "<div class=\"nsp-notification\"><div class=\"notification-container\" ng-style=\"{'top': settings.kendoOptions.position.top, 'right': settings.kendoOptions.position.right, 'bottom': settings.kendoOptions.position.bottom, 'left': settings.kendoOptions.position.left}\"></div><span kendo-notification=\"widget\" k-options=\"settings.kendoOptions\"></span></div>"
  );


  $templateCache.put('/notification/templates/default.html',
    "<span class=\"close f-nsp f-nsp-remove\" ng-click=\"close($event)\"></span><h3 class=\"title\" ng-if=\"dataItem.title\">{{dataItem.title}}</h3><div class=\"content\">{{dataItem.content}}</div>"
  );


  $templateCache.put('/nsp-grid/templates/nsp-grid.template.html',
    "<div class=\"toolbar\"><div class=\"search\"><input class=\"search-input\" id=\"{{settings.gridId + 'Search'}}\" kendo-auto-complete=\"searchWidget\" k-options=\"settings.searchOptions\" ng-model=\"settings.searchText\" ng-keydown=\"$event.which === 13 && searchGrid()\" ng-keyup=\"$event.which === 8 && settings.searchText == '' && clearSearch()\"> <a class=\"k-icon k-i-search\" ng-click=\"searchGrid()\"></a></div><button type=\"button\" class=\"refresh-btn\" ng-click=\"refreshGrid()\" title=\"{{t('Refresh', 'Common.Refresh')}}\"><i class=\"f-nsp f-nsp-refresh\"></i></button> <button type=\"button\" class=\"reset-filters-btn toolbar-btn-transparent\" ng-click=\"reset()\" ng-if=\"showReset\">{{t('Reset all filters', 'Common.ResetAllFilters')}}</button></div><div kendo-grid=\"gridWidget\" k-options=\"settings.gridOptions\" id=\"{{settings.gridId}}\" class=\"grid-container\"></div>"
  );

}]);


/****************************************
    FILE: src/components/comments/nsp-comments.service.js
****************************************/

angular.module('nspCore').factory('nspCommentsService', ['$http', '$q', 'translate', '$templateCache', 'kendoWindowService', 'appData', function($http, $q, translate, $templateCache, kendoWindowService, appData) {
    var factory = {};

    factory.commentAction = function(params, url) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: url,
            dataType: 'json',
            data: params,
            header: "application/json"
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (error, status) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    factory.getCommentTemplate = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: "/SimpleTicketView/GetSelectedTemplateData",
            data: params,
            header: "application/json"
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (error, status) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    factory.getTagValue = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: "/SimpleTicketView/AppendTicketTag",
            data: params,
            header: "application/json"
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (error, status) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    // Kendo Editor Options
    factory.editorOptions = {
        resizable: false,
        tools: [{
            name: "bold",
            tooltip: translate('Bold', 'Bold')
        }, {
            name: "italic",
            tooltip: translate('Italic', 'Italic')
        }, {
            name: "underline",
            tooltip: translate('Underline', 'Underline')
        }, {
            name: "strikethrough",
            tooltip: translate('Strikethrough', 'Strikethrough')
        }, {
            name: "insertUnorderedList",
            tooltip: translate('InsertUnorderedList', 'Insert unordered list')
        }, {
            name: "insertOrderedList",
            tooltip: translate('InsertOrderedList', 'Insert ordered list')
        }, {
            name: "createLink",
            tooltip: translate('CreateLink', 'Insert hyperlink')
        }, {
            name: "unlink",
            tooltip: translate('Unlink', 'Unlink')
        }, {
            name: "fontName",
            tooltip: translate('FontName', 'Font name')
        }, {
            name: "fontSize",
            items: [{
                text: '11px',
                value: '11px'
            }, {
                text: '13px',
                value: '13px'
            }, {
                text: '16px',
                value: '16px'
            }, {
                text: '19px',
                value: '19px'
            }, {
                text: '24px',
                value: '24px'
            }, {
                text: '32px',
                value: '32px'
            }, {
                text: '48px',
                value: '48px'
            }],
            tooltip: translate('FontSize', 'Font size')
        }, {
            name: "preview",
            template: '<a href="" role="button" class="k-tool preview" unselectable="on"><span unselectable="on" class="k-preview">' + translate('Preview', 'Common.Preview') + '</span></a>',
            exec: function(event) {
                var editor = $(this).data('kendoEditor'),
                    editorScope = $(editor.element[0]).scope(),
                    value = editor.value();

                var kendoWindowOptions = {
                    title: translate("Comment Preview", "Common.CommentPreview"),
                    modal: true,
                    width: 600,
                    maxHeight: 600,
                    content: {
                        template: $templateCache.get('/comments/nsp-comment-preview.html')
                    }
                };

                var windowData = {
                    message: value
                };

                if (value) {
                    kendoWindowService.create('commentPreview', windowData, kendoWindowOptions);
                } else {
                    editorScope.ngModel.$setValidity('tv4-302', false);
                    editorScope.$broadcast('schemaFormValidate');
                }   

            }
        }, {
            name: "insertTags",
            template: '<div insert-comment-tag class="insert-comment-tag"></div>'
        }, {
            name: "selectTemplate",
            template: '<div select-comment-template class="select-comment-template"></div>'
        }]
    };

    factory.enableTimer = appData.person ? appData.person.enableTimer : false;
    factory.enableAutoTimer = appData.person ? appData.person.enableAutoTimer : false;

    if (factory.enableTimer) {
        factory.editorOptions.tools.push({
            name: "timer",
            template: '<span nsp-comment-timer class="comment-timer"></span>'
        });
        if (factory.enableAutoTimer) {
            factory.editorOptions.keyup = function(event) {
                var editorScope = $(event.sender.element[0]).scope(),
                    value = this.value();
                if (value) {
                    editorScope.$broadcast('autoTimerStart');
                }
            };
        }
    }

    return factory;
}]);

/****************************************
    FILE: src/components/entity-properties/web/entity-properties.service.js
****************************************/

(function() {
    'use strict';

    angular
        .module('nspCore')
        .service('nspCoreEntityPropertyService', nspCoreEntityPropertyService);

    nspCoreEntityPropertyService.$inject = [];

    /* @ngInject */
    function nspCoreEntityPropertyService() {

        var factory = {};

        var templates = {
            'NSPCheckBoxProperty': '/entity-properties/web/templates/check-box.html',
            'NSPCheckBoxesProperty': '/entity-properties/web/templates/check-boxes.html',
            'NSPHelpProperty': '/entity-properties/web/templates/textarea.html',
            'NSPKendoAutoCompleteProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoComboBoxProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoDatePickerProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoDateTimePickerProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoDropDownListProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoEditorProperty': '/entity-properties/web/templates/textarea.html',
            'NSPKendoNumericTextBoxProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoTimePickerProperty': '/entity-properties/web/templates/text.html',
            'NSPRadiosProperty': '/entity-properties/web/templates/radio-button.html',
            'NSPTextProperty': '/entity-properties/web/templates/text.html',
            'string': '/entity-properties/web/templates/text.html',
            'int': '/entity-properties/web/templates/text.html',
            'boolean': '/entity-properties/web/templates/check-box.html',
            'textarea': '/entity-properties/web/templates/textarea.html',
            'image': '/entity-properties/web/templates/image.html',
            'date': '/entity-properties/web/templates/text.html',
            'time': '/entity-properties/web/templates/text.html',
            'datetime': '/entity-properties/web/templates/text.html',
            'popup': '/entity-properties/web/templates/popup.html',
            'info': '/entity-properties/web/templates/textarea.html'
        },
        popupTemplates = {
            'product': '/entity-properties/web/entity-property-product-popup.html',
            'vendor': '/entity-properties/web/entity-property-vendor-popup.html',
            'location': '/entity-properties/web/entity-property-location-popup.html',
            'usedby': '/entity-properties/web/entity-property-user-popup.html',
            'managedby': '/entity-properties/web/entity-property-user-popup.html',
            'requester': '/entity-properties/web/entity-property-user-popup.html',
            'approver': '/entity-properties/web/entity-property-user-popup.html'
        },
        popupUrl = {
            'usedby': '/UserManagement/GetPerson',
            'managedby': '/UserManagement/GetPerson',
            'requester': '/UserManagement/GetPerson',
            'approver': '/UserManagement/GetPerson',
            'location': '/UserManagement/GetAddress',
            'vendor': '/CmdbVendor/GetVendor',
            'product': '/CmdbProduct/GetProduct'
        },
        formats = {
            'NSPKendoDatePickerProperty': 'd',
            'NSPKendoTimePickerProperty': 't',
            'NSPKendoDateTimePickerProperty': 'g',
            'date': 'd',
            'time': 't',
            'datetime': 'g'
        };
        
        factory = {
            templates: templates,
            popupTemplates: popupTemplates,
            popupUrl: popupUrl,
            formats: formats
        };

        return factory;
    }
})();

/****************************************
    FILE: src/components/kendo-window/kendo-window.service.js
****************************************/

angular.module('nspCore').factory('kendoWindowService', ['$timeout', '$compile', '$templateCache', function($timeout, $compile, $templateCache) {
    var factory = {
        dialogs: {}
    };

    var defaultOptions = {
        title: "Window",
        modal: true,
        width: 400,
        openWindowManually: false,
        content: {
            url: false,
            template: $templateCache.get('/kendo-window/delete-window.html')
        },
        resizable: false,
        deactivate: function () {
            var dialogId = this.element[0].id,
                dialogScopeElem = $('#' + dialogId + ' > .ng-scope');

            if (factory.dialogs[dialogId].onWindowClose) {
                factory.dialogs[dialogId].onWindowClose(factory.dialogs[dialogId]);
            }
            if (dialogScopeElem.length) {
                dialogScopeElem.scope().$destroy();
            }
            factory.remove(dialogId);
        },
        refresh: function() {
            var that = this,
                content = that.element;
            $timeout(function() {
                var contentScope = content.scope();
                $compile(content.contents())(contentScope);
                contentScope.$digest();

                if (!that.options.openWindowManually) {
                    that.center();
                    that.open();
                }
            }, 0);
        }
    };

    factory.create = function(id, data, opts, callback) {

        // Prevent duplicate kendo window creation
        if ($('#' + id).length) {
            return false;
        }

        if (opts.content && (opts.content.url || opts.content.template)) {
            delete defaultOptions.template;
        }

        var dialog = document.createElement("div"),
            options = $.extend({}, defaultOptions, opts);
            
        dialog.setAttribute("id", id);
        $(dialog).kendoWindow(options);
        factory.dialogs[id] = data;
        if (callback) {
            callback();
        }
        $("#" + id).parent().addClass("kendo-window");
    };

    factory.read = function(id) {
        return factory.dialogs[id];
    };

    factory.remove = function(id) {
        var dialog = $('#' + id).data('kendoWindow');
        dialog.destroy();
        if (factory.dialogs[id].onWindowRemove) {
            factory.dialogs[id].onWindowRemove(factory.dialogs[id]);
        }
        delete factory.dialogs[id];
    };

    return factory;
}]);

/****************************************
    FILE: src/components/nsp-grid/nsp-grid.service.js
****************************************/

(function() {
    'use strict';

    nspGridService.$inject = ["nspCoreService"];
    angular
        .module('nspCore')
        .factory('nspGridService', nspGridService);

    /* @ngInject */
    function nspGridService(nspCoreService) {
        var factory = {
            gridParameterMap: gridParameterMap,
            searchParameterMap: searchParameterMap
        };
        return factory;

        /**
         * Grid parameter map
         * @param  {object} data       Parameter data
         * @return {object}            Request parameters
         */
        function gridParameterMap(data) {
            var params = data.params,
                result = params;
            result.pageIndex = params.page; // Set page index

            // Set sort criteria
            if (params.sort && params.sort.length) {
                result.sortCriteria = JSON.stringify(params.sort);
            }

            // Set filter criteria
            if (params.filter) {
                result.filterCriteria = angular.toJson(result.filter);
            }

            // Set searchable columns
            result.searchableColumns = getSearchableColumns(data.settings);

            // Set search text
            if (data.settings.searchText) {
                result.searchText = data.settings.searchText;
            }

            return result;
        }

        /**
         * Search parameter map
         * @param  {object} data     Parameter data
         * @return {object}          Request parameters
         */
        function searchParameterMap(data) {
            var result = data.params;

            // Set searchText and filterCriteria from filters
            if (result && result.filter) {
                for (var i = 0; i < result.filter.filters.length; i++) {
                    var currentFilter = result.filter.filters[i];
                    if (currentFilter.field === '') {
                        currentFilter.field = 'grid-search-all';
                        result.searchText = currentFilter.value;
                        break;
                    }
                }

                // Add grid filters
                if (data.gridFilters && data.gridFilters.filters && data.gridFilters.filters.length && data.settings) {
                    data.gridFilters.filters = addFiltersDbType(data.gridFilters.filters, data.settings.gridOptions);
                    for (i = 0; i < data.gridFilters.filters.length; i++) {
                        result.filter.filters.push(data.gridFilters.filters[i]);
                    }
                }
                result.filterCriteria = JSON.stringify(result.filter);
            }
            
            result.searchableColumns = getSearchableColumns(data.settings);
            return result;
        }

        /**
         * Gets searchable columns from NSP grid settings
         * @param  {object} settings NSP Grid full settings
         * @return {array}           List of searchable column field names
         */
        function getSearchableColumns(settings) {
            var result = [];
            // Set searchable columns from gridOptions
            if (settings.gridOptions.columns && settings.gridOptions.columns.length) {
                angular.forEach(settings.gridOptions.columns, function(column, key){
                    if (column.searchable) {
                        result.push(column.field);
                    }
                });
            }
            // Set searchable columns from NSP grid settings.
            // Note: This option overrides other searchableColumns.
            if (settings.searchableColumns) {
                result = settings.searchableColumns;
            }
            return result;
        }

        /**
         * Adds dbType to filters using field model.
         * @param {array}  filtersData Filters
         * @param {object} gridOptions Grid settings
         */
        function addFiltersDbType(filtersData, gridOptions) {
            function iterateForm(data) {
                for (var i = 0; i < data.length; i++) {
                    var current = data[i];
                    if (current.field === '') {
                        continue;
                    }
                    if (current.filters && current.filters.length) {
                        iterateForm(current.filters);
                        continue;
                    }
                    if (nspCoreService.propertyExists(gridOptions, 'dataSource.schema.model.fields')) {
                        if (gridOptions.dataSource.schema.model.fields[current.field]) {
                            data[i].dbType = gridOptions.dataSource.schema.model.fields[current.field].dbType;
                        }
                    }
                }
            }
            iterateForm(filtersData);
            return filtersData;
        }
    }
})();

/****************************************
    FILE: src/components/comments/nsp-comments-attachment-list.directive.js
****************************************/

angular.module('nspCore').directive('nspCommentAttachmentsList', ['$http', 'translate', 'appData', 'nspCommentsService', '$filter', 'kendoWindowService', function ($http, translate, appData, nspCommentsService, $filter, kendoWindowService) {
    return {
        scope: {
            items: "=commentAttachmentsListItems"
        },
        restrict: 'AE',
        replace: true,
        templateUrl: '/comments/nsp-comment-attachment-list.html',
        link: function (scope, element, attrs) {

            scope.attachmentImageSrc = function (attachment) {
                var imageSourceIcon = attachment.imageSourceIcon,
                    src = "";
                if (imageSourceIcon) {
                    src = imageSourceIcon;
                } else {
                    src = '/Components/GetImageThumbnailById?imageId=' + attachment.blobFileStorageId;
                }
                return src;
            };

            scope.downloadAttachment = function (attachment) {
                var url = "/SelfServicePortal/DownloadFile?blobFileStorageId=" + attachment.blobFileStorageId + "&downloadParent=true";
                window.location = url;
            };

        }
    };
}]);

/****************************************
    FILE: src/components/comments/nsp-comments-form.directive.js
****************************************/

angular.module('nspCore').directive('nspCommentsForm', ['$http', 'translate', 'appData', 'nspCommentsService', 'nspCoreService', '$filter', '$q', function($http, translate, appData, nspCommentsService, nspCoreService, $filter, $q){
    return {
        name: 'commentsForm',
        scope: {
            options: "=commentsFormOptions"
        },
        restrict: 'AE',
        replace: true,
        templateUrl: '/comments/nsp-comments-form.html',
        link: function(scope, element, attrs) {         

            var defaultOptions = {
                timer: false,
                insertTags: false,
                selectTemplate: false,
                attachmentParamsFlat: true,
                publicMark: true,
                privateMark: true,
                solutionMark: false,
                workaroundMark: false
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = translate;
            scope.formId = scope.settings.id ? scope.settings.id : "commentForm";
            scope.timeSpent = 0;

            var editorOptions = angular.copy(nspCommentsService.editorOptions);

            for (var i = 0; i < editorOptions.tools.length; i++) {
                if ((!scope.settings.insertTags && editorOptions.tools[i].name === "insertTags") ||
                    (!scope.settings.selectTemplate && editorOptions.tools[i].name === "selectTemplate") ||
                    (!scope.settings.timer && editorOptions.tools[i].name === "timer")) {
                    editorOptions.tools.splice(i, 1);
                    i--;
                }
            }

            var commentFieldKey = scope.formId + "Comment",
                schema = {
                    "type": "object",
                    "properties": {}
                },
                form = [{
                    "key": commentFieldKey,
                    "type": "kendo-editor",
                    "title": "Comment",
                    "titleKey": "Common.Comment",
                    "required": true,
                    "kendoOptions": editorOptions,
                    "onChange": function(modelValue) {
                        scope.newComment.comment = modelValue;
                    }
                }],
                model = {};

            schema.properties[scope.formId + 'Comment'] = {
                "type": "string"
            };

            model[commentFieldKey] = "";

            scope.schema = schema;
            scope.form = form;
            scope.model = model;
            scope.newComment = {
                timeSpent: 0,
                comment: "",
                isPublic: appData.person ? appData.person.ispublicComment : false,
                isPrivate: appData.person ? !appData.person.ispublicComment : true,
                isSolution: false,
                isWorkaround: false
            };
            scope.uploadAttachmentsOptions = scope.settings.uploadAttachmentsOptions;

            scope.newCommentMark = function(mark) {
                switch(mark) {
                    case "public":
                        scope.newComment.isPublic = true;
                        scope.newComment.isPrivate = false;
                    break;
                    case "private":
                        scope.newComment.isPublic = false;
                        scope.newComment.isPrivate = true;
                    break;
                    case "solution":
                        scope.newComment.isSolution = !scope.newComment.isSolution;
                        scope.newComment.isWorkaround = false;
                    break;
                    case "workaround":
                        scope.newComment.isSolution = false;
                        scope.newComment.isWorkaround = !scope.newComment.isWorkaround;
                    break;
                }
            };

            /**
             * Form Submit event handler used to validate the form
             */
            scope.onCommentSubmit = function() {
                var form = scope[scope.formId];
                scope.$broadcast('schemaFormValidate');
                if (form.$valid) {
                    scope.submitComment();
                } else {
                    if(window.nspSendCommonNotification){
                        if (window.nspSendCommonNotification.error) {
                            window.nspSendCommonNotification.error({
                              notificationHeader: '',
                              notificationMessage: translate('Comment submit failed.', 'Common.CommentSubmitFailed')
                            });
                        }
                    }  
                }
            };

            /**
             * Submits Comment to back-end
             */
            scope.submitComment = function() {
                var params = {
                    webBrowser: nspCoreService.getBrowser(),
                    comment: scope.newComment
                };
                params = $.extend({}, params, scope.settings.params);

                if (scope.settings.attachmentParamsFlat && params.comment.attachments) {
                    var attachments = [];
                    for (var i = 0; i < params.comment.attachments.length; i++) {
                        attachments.push(params.comment.attachments[i].blobFileStorageId);
                    }
                    params.attachmentIds = attachments;
                }

                // UtilityService.Loader.Transparent.show();
                scope.$broadcast('timerStop');
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: scope.settings.url,
                    dataType: 'json',
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                })
                .success(function(response) {
                    // Reset Form after successful submit
                    scope.model[commentFieldKey] = "";
                    scope.newComment.timeSpent = 0;
                    scope.newComment.comment = "";
                    scope.newComment.attachments = [];
                    scope.$broadcast('fileDropReset');
                    scope.$broadcast('schemaFormRedraw');
                    if (angular.isFunction(scope.settings.onSubmit)) {
                        scope.settings.onSubmit(response);
                    }
                    // UtilityService.Loader.Transparent.hide();
                    deferred.resolve(response);
                })
                .error(function(err, status) {
                    if(window.nspSendCommonNotification){
                        if (window.nspSendCommonNotification.error) {
                          window.nspSendCommonNotification.error({
                            notificationHeader: '',
                            notificationMessage: translate('Comment submit failed.', 'Common.CommentSubmitFailed')
                          });
                        }
                    }                    
                    // UtilityService.Loader.Transparent.hide();
                    deferred.reject(err);
                    throw new Error("Failed to submit comment: " + err);
                }); 
                return deferred.promise;
            };

            scope.$on('commentTimerChanged', function(event, value) {
                if (value > 0 && value < 59) {
                    value = 60;
                }
                scope.newComment.timeSpent = value;
            });

        }
    };
}]);

/****************************************
    FILE: src/components/comments/nsp-comments-list.directive.js
****************************************/

angular.module('nspCore').directive('nspCommentsList', ['$http', 'nspCoreService', '$templateCache', 'appData', 'nspCommentsService', '$filter', 'kendoWindowService', function($http, nspCoreService, $templateCache, appData, nspCommentsService, $filter, kendoWindowService) {
    return {
        name: 'commentsList',
        scope: {
            options: "=commentsListOptions"
        },
        restrict: 'AE',
        templateUrl: '/comments/nsp-comments-list.html',
        link: function(scope, element, attrs) {

            var defaultOptions = {
                publicMark: true,
                privateMark: true,
                solutionMark: false,
                workaroundMark: false
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = nspCoreService.translate;
            scope.commentsList = [];
            scope.allCollapsed = scope.settings.collapsed;

            function getCommentIndex(id) {
                var index = 0;
                for (var i = 0; i < scope.commentsList.length; i++) {
                    if (scope.commentsList[i].id === id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }

            scope.loadComments = function() {
                // UtilityService.Loader.Transparent.show();
                $http({
                        method: 'GET',
                        url: scope.settings.url,
                        dataType: 'json',
                        params: scope.settings.params
                    })
                    .success(function(response) {
                        var data = response;
                        angular.forEach(data.list, function(value, key) {
                            value.comment = nspCoreService.htmlDecode(value.comment);
                            value.collapsed = scope.allCollapsed;
                        });
                        scope.commentsList = data.list;
                        if (scope.options.commentListLength) {
                            scope.options.commentListLength(data.totalCount);
                        }
                        // UtilityService.Loader.Transparent.hide();
                    })
                    .error(function(error, status) {
                        if (window.nspSendCommonNotification.error) {
                            window.nspSendCommonNotification.error({
                                notificationHeader: '',
                                notificationMessage: nspCoreService.translate('Failed to load comments.', 'Common.FailedToLoadComments')
                            });
                        }
                        // UtilityService.Loader.Transparent.hide();
                        throw new Error("Failed to load comments: " + error);
                    });
            };

            scope.timeAgo = function(value, format) {
                return nspCoreService.timeAgoLegacy(value, format);
            };

            scope.formatTimeSpent = function(value) {
                return nspCoreService.secondsToHoursAndMinutes(value);
            };

            scope.changeCommentMark = function(id, mark) {
                var index = getCommentIndex(id);
                switch (mark) {
                    case "public":
                        scope.commentsList[index].isPublic = true;
                        scope.commentsList[index].isPrivate = false;
                        break;
                    case "private":
                        scope.commentsList[index].isPublic = false;
                        scope.commentsList[index].isPrivate = true;
                        break;
                    case "solution":
                        scope.commentsList[index].isSolution = !scope.commentsList[index].isSolution;
                        scope.commentsList[index].isWorkaround = false;
                        break;
                    case "workaround":
                        scope.commentsList[index].isSolution = false;
                        scope.commentsList[index].isWorkaround = !scope.commentsList[index].isWorkaround;
                        break;
                }
                nspCommentsService.commentAction({
                    "id": scope.commentsList[index].id,
                    "isPublic": scope.commentsList[index].isPublic,
                    "isPrivate": scope.commentsList[index].isPrivate,
                    "isSolution": scope.commentsList[index].isSolution,
                    "isWorkaround": scope.commentsList[index].isWorkaround
                }, scope.settings.execUrl);
            };

            scope.likeComment = function(id) {
                var index = getCommentIndex(id);
                if (scope.commentsList[index].isLiked) {
                    return false;
                }
                scope.commentsList[index].likes++;
                scope.commentsList[index].isLiked = true;
                nspCommentsService.commentAction({
                    "id": scope.commentsList[index].id,
                    "isPublic": scope.commentsList[index].isPublic,
                    "isPrivate": scope.commentsList[index].isPrivate,
                    "isSolution": scope.commentsList[index].isSolution,
                    "isWorkaround": scope.commentsList[index].isWorkaround,
                    "isLiked": true
                }, scope.settings.execUrl);
            };

            scope.collapseToggle = function(id) {
                var index = getCommentIndex(id);
                scope.commentsList[index].collapsed = !scope.commentsList[index].collapsed;
            };

            scope.collapseToggleAll = function() {
                scope.allCollapsed = !scope.allCollapsed;
                angular.forEach(scope.commentsList, function(value, key) {
                    value.collapsed = scope.allCollapsed;
                });
            };

            scope.countComments = function(mark) {
                var count = 0;
                switch (mark) {
                    case "solution":
                        count = $filter('filter')(scope.commentsList, {
                            isSolution: true
                        }).length;
                        break;
                    case "workaround":
                        count = $filter('filter')(scope.commentsList, {
                            isWorkaround: true
                        }).length;
                        break;
                    case "attachments":
                        count = $filter('filter')(scope.commentsList, {
                            hasAttachment: true
                        }).length;
                        break;
                    case "all":
                        count = scope.commentsList.length;
                        break;
                }
                return count;
            };

            scope.markFilter = function(filter) {
                scope.activeMarkFilter = scope.activeMarkFilter === filter || !filter ? "" : filter;
            };

            scope.mapArticle = function(item) {
                var kendoWindowOptions = {
                    title: nspCoreService.translate('New Article From Ticket', 'Common.NewArticleFromTicket'),
                    modal: true,
                    width: 800,
                    height: "90%",
                    maxHeight: 460,
                    content: {
                        url: "/Scripts/build/app/views/knowledge-base/article-mapping-form.html"
                    }
                };
                var windowData = {
                    "comment": item,
                    "ticketId": scope.options.params.ticketId,
                    "ticketTypeId": scope.$parent.data.ticketTypeId,
                    "commentId": item.id,
                    "formId": scope.options.formId 
                };

                kendoWindowService.create('ArticleMappingForm', windowData, kendoWindowOptions);
            };

            scope.attachmentImageSrc = function(attachment) {
                var imageSourceIcon = attachment.imageSourceIcon,
                    src = "";
                if (imageSourceIcon) {
                    src = imageSourceIcon;
                } else {
                    src = '/Components/GetImageThumbnailById?imageId=' + attachment.blobFileStorageId;
                }
                return src;
            };

            scope.downloadAttachment = function(attachment) {
                var url = "/Components/DownloadFile?blobFileStorageId=" + attachment.blobFileStorageId + "&downloadParent=true";
                window.location = url;
            };

            scope.sendEmail = function(item) {
                var kendoWindowOptions = {
                    title: nspCoreService.translate('Send Email', 'Common.SendEmail'),
                    modal: true,
                    width: 800,
                    height: "90%",
                    maxHeight: 460,
                    content: {
                        template: $templateCache.get('/comments/nsp-send-email-comment.html')
                    }
                };
                var windowData = {
                    comment: item,
                    additionalData: scope.settings.additionalData,
                    attachments: scope.commentsList
                };

                kendoWindowService.create('commentSendAsEmail', windowData, kendoWindowOptions);
            };

            scope.$on('loadComments', function() {
                // UtilityService.Loader.Transparent.show();
                scope.loadComments();
            });
            scope.loadComments();

        }
    };
}]);

angular.module('nspCore').filter('filterComments', ["$filter", function($filter) {
    return function(items, searchValue, activeMarkFilter) {
        var result = [];
        result = searchValue ? $filter('filter')(items, {
            comment: searchValue
        }) : items;
        switch (activeMarkFilter) {
            case "solution":
                result = $filter('filter')(result, {
                    isSolution: true
                });
                break;
            case "workaround":
                result = $filter('filter')(result, {
                    isWorkaround: true
                });
                break;
            case "attachments":
                result = $filter('filter')(result, {
                    hasAttachment: true
                });
                break;
        }
        return result.slice().reverse();
    };
}]);

/****************************************
    FILE: src/components/comments/nsp-comments-timer.directive.js
****************************************/

angular.module('nspCore').directive('nspCommentTimer', ['$http', 'translate', 'appData', 'nspCommentsService', 'nspCoreService', '$interval', '$timeout', function ($http, translate, appData, nspCommentsService, nspCoreService, $interval, $timeout) {
    return {
        name: 'nspCommentTimer',
        scope: {},
        restrict: 'AE',
        templateUrl: '/comments/nsp-comment-timer.html',
        link: function (scope, element, attrs) {

            var startText = translate("Start", "Common.Start"),
                stopText = translate("Stop", "Common.Stop"),
                timerInterval;

            scope.toggleBtnText = startText;
            scope.t = translate;
            scope.running = false;
            scope.timerPattern = /^[0-9]{1,3}[:][0-5]?[0-9]$/;
            scope.timerInput = "";
            scope.seconds = 0;

            scope.toggle = function ($event) {
                if (scope.running) {
                    scope.stopTimer();
                } else {
                    scope.startTimer();
                }
                $event.preventDefault();
            };

            scope.inputChanged = function () {
                var value = scope.timerInput,
                    hours = 0,
                    minutes = 0;
                if (value && scope.timerForm.timerInput.$valid) {
                    hours = parseInt(value.split(':')[0]);
                    minutes = parseInt(value.split(':')[1]);
                    scope.seconds = hours * 3600 + minutes * 60;
                } else if (scope.seconds !== 0) {
                    scope.seconds = 0;
                }
            };

            scope.startTimer = function () {
                scope.running = true;
                scope.toggleBtnText = stopText;
                scope.timerInput = scope.timerInput ? scope.timerInput : "00:01";
                if (timerInterval) {
                    $interval.cancel(timerInterval);
                }
                scope.seconds++;
                timerInterval = $interval(function () {
                    scope.seconds++;
                }, 1000);
                $timeout(function () {
                    scope.$digest();
                }, 0);
            };

            scope.stopTimer = function () {
                scope.running = false;
                scope.toggleBtnText = startText;
                if (timerInterval) {
                    $interval.cancel(timerInterval);
                }
            };

            scope.timerInputBlur = function () {
                if (scope.timerInput && scope.timerForm.timerInput.$valid) {
                    scope.timerInput = nspCoreService.secondsToHoursAndMinutes(scope.seconds);
                    scope.startTimer();
                } else {
                    scope.timerInput = "";
                    scope.stopTimer();
                }
            };

            scope.timerInputFocus = function () {
                scope.stopTimer();
            };

            scope.$watch('seconds', function (newValue, oldValue) {
                if (scope.running) {
                    if (newValue === 0) {
                        scope.timerInput = "";
                    } else if (newValue < 60) {
                        scope.timerInput = "00:01";
                    } else {
                        scope.timerInput = nspCoreService.secondsToHoursAndMinutes(scope.seconds);
                    }
                }
                scope.$emit('commentTimerChanged', newValue);
            });

            scope.$on('destroy', function () {
                $interval.cancel(timerInterval);
            });

            scope.$on('autoTimerStart', function () {
                if (!scope.running) {
                    scope.startTimer();
                }
            });

            scope.$on('timerStop', function () {
                if (scope.running) {
                    scope.stopTimer();
                }
            });

            element.parent().addClass('timer-tool');

        }
    };
}]);



/****************************************
    FILE: src/components/entity-properties/web/entity-properties.directive.js
****************************************/

(function() {
    'use strict';

    angular
        .module('nspCore')
        .directive('nspCoreEntityProperty', nspCoreEntityProperty);

    nspCoreEntityProperty.$inject = ['$http', '$templateCache', '$timeout', '$compile', '$sce', 'nspCoreService', 'nspCoreEntityPropertyService', 'kendoWindowService'];

    /* @ngInject */
    function nspCoreEntityProperty($http, $templateCache, $timeout, $compile, $sce, nspCoreService, nspCoreEntityPropertyService, kendoWindowService) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                options: "=options"
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var templateUrl = nspCoreEntityPropertyService.templates[scope.options.nspType],
                defaultOptions = {};

            scope.t = nspCoreService.translate;

            switch(scope.options.nspType) {
                case 'NSPTextProperty':
                case 'NSPKendoAutoCompleteProperty':
                case 'NSPKendoDropDownListProperty':
                case 'NSPKendoComboBoxProperty':
                    scope.propertyType = 'text';
                    if (scope.options.viewType && scope.options.viewType === 'popup') {
                        scope.propertyType = 'popup';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.viewType];
                    }
                    break;
                case 'NSPKendoTimePickerProperty':
                case 'NSPKendoDatePickerProperty':
                case 'NSPKendoDateTimePickerProperty':
                    scope.propertyType = 'date-time';
                    var parsedDate = kendo.parseDate(scope.options.value);
                    scope.options.value = kendo.toString(parsedDate, nspCoreEntityPropertyService.formats[scope.options.nspType]);
                    break;
                case 'NSPRadiosProperty':
                    scope.options.value = scope.options.type === 'string' ? JSON.parse(scope.options.value) : scope.options.value;
                    scope.propertyType = 'radio-button';
                    break;
                case 'NSPCheckBoxesProperty':
                    scope.options.value = scope.options.type === 'string' ? JSON.parse(scope.options.value) : scope.options.value;
                    scope.propertyType = 'check-boxes';
                    break;
                case 'NSPCheckBoxProperty':
                    scope.propertyType = 'check-box';
                    scope.options.value = scope.options.value === true || scope.options.value.toUpperCase() === 'TRUE' ? true : false;
                    break;
                case 'NSPKendoEditorProperty':
                    scope.propertyType = 'textarea';
                    break;
                case 'NSPKendoNumericTextBoxProperty':
                    scope.propertyType = 'number';
                    break;
                case 'NSPHelpProperty':
                    scope.propertyType = 'help';
                    break;
                default:
                    if (scope.options.type === 'string' || scope.options.type === 'int') {
                        scope.propertyType = 'text';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'date' || scope.options.type === 'time' || scope.options.type === 'datetime') {
                        scope.propertyType = 'date-time';
                        var parseDate = kendo.parseDate(scope.options.value);
                        scope.options.value = kendo.toString(parseDate, nspCoreEntityPropertyService.formats[scope.options.type]);
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'boolean') {
                        scope.propertyType = 'check-box';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'image') {
                        scope.propertyType = 'image';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'info' || scope.options.type === 'textarea') {
                        scope.propertyType = 'textarea';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else {
                        scope.propertyType = 'text';
                        templateUrl = nspCoreEntityPropertyService.templates.NSPTextProperty;
                    }
            }

            scope.trustHtml = function(string){
                return $sce.trustAsHtml(string);
            };

            scope.openPopup = function() {
                if (scope.options.id) {
                    var title;
                    if (scope.options.entityType === 'usedby' || scope.options.entityType === 'managedby' || scope.options.entityType === 'requester' || scope.options.entityType === 'approver') {
                        title = nspCoreService.translate('User', 'Common.User');
                    }

                    var kendoWindowOptions = {
                        title: title ? title : nspCoreService.translate(scope.options.label, scope.options.labelKey),
                        modal: true,
                        width: 830,
                        height: 330,
                        content: {
                            url: false,
                            template: $templateCache.get(nspCoreEntityPropertyService.popupTemplates[scope.options.entityType])
                        }
                    };

                    var windowData = {
                        data: scope.options
                    };

                    kendoWindowService.create('entityPropertyPopup', windowData, kendoWindowOptions);
                }    
            };

            scope.options = angular.merge({}, defaultOptions, scope.options);

            element.html($templateCache.get(templateUrl));
            $compile(element.contents())(scope);
            
        }
    }
})();

/****************************************
    FILE: src/components/kendo-window/delete-window.directive.js
****************************************/

angular.module('nspCore').directive('deleteWindow', ['$http', 'UtilityService', 'kendoWindowService', 'translate', function($http, UtilityService, kendoWindowService, translate) {
    return {
        restrict: 'AE',
        scope: true,
        link: function(scope, element, attrs) {

            var dialogId = element[0].parentElement.id,
                dialog = $('#' + dialogId).data('kendoWindow');
            dialogData = kendoWindowService.read(dialogId);
            var successNotification,
                errorNotification;
            scope.data = dialogData;
            if (scope.data.isRemove) {
                scope.message = scope.data.message ? scope.data.message : translate('Are you sure you want to remove?', 'Common.RemovalQuestion');
                successNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('Removed Successfully', 'Common.SuccessfulRemoval')
                };
                errorNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('This item cannot be removed', 'Common.FailedRemoval')
                };
            } else {
                scope.message = scope.data.message ? scope.data.message : translate('Are you sure you want to delete?', 'CiStates.DelMess');
                // generic success notification
                successNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('Deleted Successfully', 'DeletedSuccessfully')
                };
                // generic error notification
                errorNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('This item cannot be deleted.', 'Common.ThisItemCannotBeDeleted')
                };
            }
            scope.confirmButtonText = dialogData.confirmButtonText ? dialogData.confirmButtonText : translate('Yes', 'Yes');
            scope.cancelButtonText = dialogData.cancelButtonText ? dialogData.cancelButtonText : translate('No', 'No');

            // override success notification options if they are customized
            if (dialogData.successNotification) {
                angular.extend(successNotification, dialogData.successNotification);
            }

            // override error notification options if they are customized
            if (dialogData.errorNotification) {
                angular.extend(errorNotification, dialogData.errorNotification);
            }

            scope.delete = function() {

                UtilityService.Loader.Transparent.show();
                if (scope.data.deleteUrl) {
                    $http({
                            method: 'POST',
                            url: scope.data.deleteUrl,
                            data: scope.data.params ? $.param(scope.data.params) : null,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                        .success(function(response) {
                            UtilityService.Loader.Transparent.hide(0);
                            if (response == 'True' || response == '-13' || response == 'true' || response === true) {
                                if (scope.data.onSuccess) {
                                    scope.data.onSuccess(response);
                                }

                                window.nspSendCommonNotification.success(successNotification);
                            }
                            if (response == 'False' || response == '-14' || response == '-15' || response == 'false' || response === false) {
                                if (scope.data.onFailed) {
                                    scope.data.onFailed(response);
                                }

                                window.nspSendCommonNotification.error(errorNotification);
                            }
                            dialog.close();
                        })
                        .error(function(response) {
                            UtilityService.Loader.Transparent.hide(0);
                        });
                } else {
                    if (angular.isFunction(scope.data.callback)) {
                        scope.data.callback();
                        dialog.close();
                    }
                    UtilityService.Loader.Transparent.hide(0);
                }
            };

            scope.cancel = function() {
                dialog.close();
            };
        }
    };
}]);

/****************************************
    FILE: src/components/kendo-window/kendo-window-content.directive.js
****************************************/

angular.module('nspCore').directive('kendoWindowContent', ['$http', 'kendoWindowService', function($http, kendoWindowService) {
    return {
        restrict: 'AE',
        scope: true,
        link: function(scope, element, attrs) {
            var dialogId = element[0].parentElement.id;
            scope.dialogId = dialogId;
            scope.dialogData = kendoWindowService.read(dialogId);
        }
    };
}]);

/****************************************
    FILE: src/components/notification/notification.directive.js
****************************************/

(function() {
    'use strict';

    nspNotification.$inject = ["$templateCache", "nspCoreService"];
    angular
        .module('nspCore')
        .directive('nspNotification', nspNotification);

    /* @ngInject */
    function nspNotification($templateCache, nspCoreService) {
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '/notification/notification.html',
            scope: {
                options: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var defaultOptions = {
                kendoOptions: {
                    position: {
                        pinned: true,
                        top: 20,
                        right: 20,
                        width: 200
                    },
                    stacking: 'down',
                    hideOnClick: false,
                    appendTo: '.notification-container',
                    autoHideAfter: 7000,
                    button: true,
                    templates: [
                        {
                            type: "success",
                            template: $templateCache.get('/notification/templates/default.html')
                        },
                        {
                            type: "warning",
                            template: $templateCache.get('/notification/templates/default.html')
                        },
                        {
                            type: "info",
                            template: $templateCache.get('/notification/templates/default.html')
                        },
                        {
                            type: "error",
                            template: $templateCache.get('/notification/templates/default.html')
                        }
                    ]
                }
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = nspCoreService.translate;
            scope.close = close;

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('show', onShow);
            });

            scope.$on('notify', function(event, data) {
                scope.widget.show(data, data.type);
            });

            function onShow(event) {
                event.element.addClass('nsp-notification');
            }

            function close(event) {
                event.target.closest('.k-notification').remove();
            }

        }
    }
})();

/****************************************
    FILE: src/components/nsp-grid/nsp-grid.directive.js
****************************************/

(function() {
    'use strict';

    nspGrid.$inject = ["$http", "$rootScope", "$templateCache", "$compile", "$timeout", "nspCoreService", "nspGridService"];
    angular
        .module('nspCore')
        .directive('nspGrid', nspGrid);

    /* @ngInject */
    function nspGrid($http, $rootScope, $templateCache, $compile, $timeout, nspCoreService, nspGridService) {
        var directive = {
            link: link,
            restrict: 'AE',
            scope: {
                options: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var defaultOptions = {
                template: '/nsp-grid/templates/nsp-grid.template.html',
                gridId: 'nspGrid-' + kendo.guid(),
                gridOptions: {
                    dataSource: {
                        transport: {
                            read: {
                                dataType: "json",
                                type: "POST"
                            },
                            parameterMap: function(data, type) {
                                return nspGridService.gridParameterMap({
                                    params: data,
                                    type: type,
                                    settings: scope.settings
                                });
                            }
                        },
                        schema: {
                            data: 'list',
                            total: 'totalCount'
                        },
                        serverFiltering: true,
                        serverPaging: true,
                        serverSorting: true,
                        pageSize: 20
                    },
                    resizable: true,
                    reorderable: false,
                    filterable: true,
                    columnMenu: {
                        columns: false
                    },
                    sortable: {
                        mode: "single",
                        allowUnsort: true
                    },
                    selectable: "row",
                    scrollable: false,
                    noRecords: {
                        template: "<span>" + nspCoreService.translate('No available data.', 'Common.NoAvailableData') + "</span>"
                    },
                    pageable: {
                        numeric: false,
                        pageSizes: [5, 10, 15, 20],
                        messages: {
                            display: "{0}-{1} " + nspCoreService.translate('of', 'Of') + " {2}",
                            itemsPerPage: nspCoreService.translate('Rows per page', 'Common.RowsPerPage') + ":"
                        }
                    }
                },
                search: true,
                searchText: '',
                searchOptions: {
                    dataSource: {
                        transport: {
                            read: {
                                url: "/CmdbCiView/GetAutoCompleteDataSource",
                                dataType: "json",
                                type: "POST"
                            },
                            parameterMap: function(data, type) {
                                return nspGridService.searchParameterMap({
                                    params: data,
                                    type: type,
                                    settings: scope.settings,
                                    gridFilters: scope.gridWidget.dataSource.filter() ? scope.gridWidget.dataSource.filter() : []
                                });
                            }
                        },
                        schema: {
                            data: "list"
                        },
                        serverFiltering: true
                    },
                    filter: 'contains',
                    template: "<span ng-non-bindable>#=data#</span>",
                    placeholder: nspCoreService.translate('Search...', 'SearchDotDotDot'),
                    minLength: 3,
                    delay: 500
                }
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = nspCoreService.translate;
            scope.refreshGrid = refreshGrid;
            scope.searchGrid = searchGrid;
            scope.reset = reset;
            scope.showReset = false;

            element.addClass('nsp-grid');

            element.html($templateCache.get(scope.settings.template));
            $compile(element.contents())(scope);

            scope.searchWidget.bind('select', onSearchSelect);
            scope.gridWidget.bind('dataBound', onGridDataBound);

            function onSearchSelect(event) {
                var dataItem = scope.searchWidget.dataItem(event.item.index());
                scope.settings.searchText = dataItem;
                searchGrid();
            }

            function searchGrid() {
                scope.gridWidget.dataSource.page(1);
            }

            function refreshGrid() {
                scope.gridWidget.dataSource.read().then(function() {
                    scope.gridWidget.refresh();
                });                
            }

            function reset() {
                scope.searchWidget.value('');
                scope.settings.searchText = '';
                scope.gridWidget.dataSource.query({
                    sort: getFixedSorters(),
                    filter: getFixedFilters(),
                    pageSize: scope.settings.gridOptions.dataSource.pageSize,
                    page: 1,
                    skip: 0
                });
                if (angular.isFunction(scope.settings.onReset)) {
                    scope.settings.onReset(scope);
                }
            }

            function onGridDataBound(event) {
                $timeout(function() {
                    scope.showReset = isResetVisible();
                }, 0);
            }

            function getFixedFilters() {
                var result = [],
                    gridFilters = scope.gridWidget.dataSource.filter() ? scope.gridWidget.dataSource.filter() : [];

                if (gridFilters.filters) {
                    angular.forEach(gridFilters.filters, function(filter, key) {
                        if (filter.fixed) {
                            result.push(filter);
                        }
                    });
                }
                return result;
            }

            function getFixedSorters() {
                var result = [],
                    gridSorters = scope.gridWidget.dataSource.sort() ? scope.gridWidget.dataSource.sort() : [];

                angular.forEach(gridSorters, function(sorter, key) {
                    if (sorter.fixed) {
                        result.push(sorter);
                    }
                });
                return result;
            }

            function isResetVisible() {
                var result = false,
                    gridFilters = scope.gridWidget.dataSource.filter() ? scope.gridWidget.dataSource.filter() : [],
                    gridSorters = scope.gridWidget.dataSource.sort() ? scope.gridWidget.dataSource.sort() : [];

                angular.forEach(gridSorters, function(filter, key) {
                    if (!filter.fixed) {
                        result = true;
                    }
                });

                if (gridFilters.filters) {
                    angular.forEach(gridFilters.filters, function(filter, key) {
                        if (!filter.fixed) {
                            result = true;
                        }
                    });
                }

                if (scope.settings.searchText.length) {
                    result = true;
                }
                return result;
            }
        }
    }
})();

/****************************************
    FILE: src/components/text-pane-popup-images/text-pane-popup-images.directive.js
****************************************/

angular.module('nspCore').directive('nspCoreTextPanePopupImages', [function() {
    return {
        restrict: 'AE',
        link: function(scope, element, attrs) {
            element.on('click', 'img', function() {
                var $this = $(this),
                    width = $this.width(),
                    height = $this.height(),
                    naturalWidth = this.naturalWidth,
                    naturalHeight = this.naturalHeight;
                if (width < naturalWidth || height < naturalHeight) {
                    var dialogElement = document.createElement("div");
                    var dialog = $(dialogElement).kendoWindow({
                        modal: true,
                        resizable: false,
                        width: "90%",
                        height: "80%",
                        deactivate: function () {
                            this.destroy();
                        }
                    }).data('kendoWindow');
                    dialog.content('<div style="text-align: center"><img src="' + $this.prop('src') + '" ></div>');
                    $(dialogElement).parent().addClass('kendo-window');
                    dialog.center();
                    dialog.open();
                }
            });
        }
    };
}]);

/****************************************
    FILE: src/components/comments/nsp-send-email-comment.controller.js
****************************************/

angular.module('nspCore')
  .controller('NspSendEmailCommentController', ['$scope', '$http', 'translate', 'themeService', '$compile', '$timeout', 'UtilityService', 'kendoWindowService', '$templateCache', '$sce',
    function ($scope, $http, translate, themeService, $compile, $timeout, UtilityService, kendoWindowService, $templateCache, $sce) {

        var dialogId = 'commentSendAsEmail',
          dialog = $('#' + dialogId).data('kendoWindow'),
          dialogData = kendoWindowService.read(dialogId),
          newToEmailitemtext;

        $scope.formId = "SendEmailCommentForm";

        $scope.t = function (text, key) {
            return translate(text, key);
        };

        $scope.schema = {
            "type": "object",
            "properties": {
                "recipentMailArray": {
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                },
                "sendEmailCommentFormSubject": {
                    "type": "string"
                },
                "messageContent": {
                    "type": "string"
                }
            }
        };

        $scope.form = [{
            "key": "recipentMailArray",
            "type": "kendo-multi-select",
            "title": "Recipents",
            "titleKey": "Common.Recipents",
            "required": true,
            "kendoOptions": {
                filter: 'contains',
                ignoreCase: false,
                autoBind: false,
                dataSource: {
                    type: 'jsonp',
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "/TabNavigation/GetDropDownDataOfBaseMultiSelect?instanceId=0&entityTypeId=0&dataObject=BaseEndUser&userIds=0"
                        },
                        parameterMap: function (options) {
                            return options;
                        }
                    }
                },
                dataBound: function () {
                    if ((newToEmailitemtext || this._prev) && newToEmailitemtext != this._prev) {
                        newToEmailitemtext = this._prev;

                        if (newToEmailitemtext.length > 0) {
                            if (isValidEmailAddress(newToEmailitemtext)) {
                                this.dataSource.options.serverFiltering = false;
                                this.dataSource.add({ name: newToEmailitemtext + " ", value: newToEmailitemtext, email: newToEmailitemtext });
                                this.refresh();
                                this.search();
                                this.open();
                            } else {
                                this.dataSource.options.serverFiltering = true;
                            }
                        }
                    }
                },
                dataTextField: 'name',
                dataValueField: 'value',
                dataAdditionalFields: ["email"],
                animation: false
            }
        }, {
            "key": "sendEmailCommentFormSubject",
            "type": "default",
            "title": "Subject",
            "titleKey": "Common.Subject",
            "required": true
        }, {
            "key": "messageContent",
            "type": "kendo-editor",
            "title": "Your message",
            "titleKey": "Common.YourMessage",
            "required": true,
            "kendoOptions": {
                "encoded": true
            }
        }, {
            "type": "actions",
            "items": [{
                "type": "submit",
                "title": "Done",
                "titleKey": "Done"
            }, {
                "key": "cancelButton",
                "type": "button",
                "title": "Cancel",
                "titleKey": "Common.Cancel",
                "onClick": "cancel()"
            }]
        }];

        var attachmetIds = '';
        for (var i = 0; i < dialogData.comment.attachments.length; i++) {
            if (attachmetIds === '') {
                attachmetIds = dialogData.comment.attachments[i].blobFileStorageId.toString();
            } else {
                attachmetIds = attachmetIds + ',' + dialogData.comment.attachments[i].blobFileStorageId.toString();
            }
        }


        $scope.model = {
            "referenceNumber": dialogData.additionalData.referenceNumber,
            "subject": dialogData.additionalData.subject,
            "sendEmailCommentFormSubject": dialogData.additionalData.referenceNumber + ' ' + dialogData.additionalData.subject,
            "recipentMailArray": "",
            "messageContent": $sce.getTrustedHtml(dialogData.comment.comment),
            "entityInstanceId": dialogData.additionalData.entityInstanceId,
            "entityTypeId": dialogData.additionalData.entityTypeId,
            "attachments": attachmetIds,

        };

        /*
         * Form Submit Event
         * @param  {object} form Form fields
         */
        $scope.onSubmit = function () {
            var form = $scope[$scope.formId];
            $scope.$broadcast('schemaFormValidate');

            if (form.$valid) {
                $scope.submitForm();
            } else {
                if (window.nspSendCommonNotification.error) {
                    window.nspSendCommonNotification.error({
                        notificationHeader: '',
                        notificationMessage: $scope.t('Form validation failed', 'ErrorFormValidation')
                    });
                }
            }
        };

        /*
         * Submits form to server
         * @return {[type]} [description]
         */
        $scope.submitForm = function () {
            var emails = [];
            for (var i = 0; i < $scope.model.recipentMailArray.length; i++) {
                emails.push($scope.model.recipentMailArray[i].email);
            }

            var params = {
                CommentData: $scope.model.messageContent,
                EntityInstanceId: $scope.model.entityInstanceId,
                EntityTypeId: $scope.model.entityTypeId,
                ToEmailDirect: emails,
                Subject: $scope.model.subject,
                RefrenceNo: $scope.model.referenceNumber,
                FileAttachmentListIds: $scope.model.attachments,

            };

            UtilityService.Loader.Transparent.show();

            $http({
                method: 'POST',
                url: dialogData.additionalData.urlSendEmail,
                data: params, // Pass form model
                headers: { 'Content-Type': 'application/json' }
            })
            .success(function (response) {
                UtilityService.Loader.Transparent.hide();
                if (response.success === true) {
                    if (window.nspSendCommonNotification.success) {
                        window.nspSendCommonNotification.success({
                            notificationHeader: '',
                            notificationMessage: $scope.t('Email sent', 'Common.EmailSentSuccess')
                        });
                    }
                    dialog.close();
                } else {
                    if (window.nspSendCommonNotification.error) {
                        window.nspSendCommonNotification.error({
                            notificationHeader: '',
                            notificationMessage: $scope.t('Email was not sent', 'Common.EmailSentError')
                        });
                    }
                }
            })
            .error(function (response) {
                UtilityService.Loader.Transparent.hide();
                if (window.nspSendCommonNotification.error) {
                    window.nspSendCommonNotification.error({
                        notificationHeader: '',
                        notificationMessage: $scope.t('Email was not sent', 'Common.EmailSentError')
                    });
                }
            });
        };

        $scope.cancel = function () {
            dialog.close();
        };


        $scope.attachmentImageSrc = function (attachment) {
            var imageSourceIcon = attachment.imageSourceIcon,
                src = "";
            if (imageSourceIcon) {
                src = imageSourceIcon;
            } else {
                src = '/Components/GetImageThumbnailById?imageId=' + attachment.blobFileStorageId;
            }
            return src;
        };

        $scope.downloadAttachment = function (attachment) {
            var url = "/SelfServicePortal/DownloadFile?blobFileStorageId=" + attachment.blobFileStorageId + "&downloadParent=true";
            window.location = url;
        };


        $timeout(function () {
            var recipentMailsInput = $('#' + $scope.form[0].key);
            recipentMailsInput.on('keyup', function (event) {
                if (event.keyCode === 32) {
                    var inputValue = $(event.target).val();
                    inputValue = inputValue + ' , ';
                    $(event.target).val(inputValue);
                }
            });
        }, 0);
    }]);

/****************************************
    FILE: src/components/entity-properties/web/entity-property-popup.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('nspCore')
        .controller('EntityPropertyPopupController', EntityPropertyPopupController);

    EntityPropertyPopupController.$inject = ['$scope', '$http', '$timeout', '$sce', 'nspCoreService', 'nspCoreEntityPropertyService', 'kendoWindowService'];

    /* @ngInject */
    function EntityPropertyPopupController($scope, $http, $timeout, $sce, nspCoreService, nspCoreEntityPropertyService, kendoWindowService) {
        var vm = this,
            dialogId = 'entityPropertyPopup';

        $scope.dialogData = kendoWindowService.read(dialogId);
        $scope.t = nspCoreService.translate;
        $('#' + dialogId).parent().addClass('entity-property-popup');

        var params = {
            id: $scope.dialogData.data.id
        };

        $scope.trustHtml = function(string){
            return $sce.trustAsHtml(string);
        };

        $scope.buildMap = function(lat, long) {
            var mapCanvas = document.getElementById("map-canvas");
            if (mapCanvas) {
                var mapOptions = {
                    center: new google.maps.LatLng(lat, long),
                    zoom: 10,
                    mapTypeControlOptions: {
                        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                    }
                };
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({
                    position: mapOptions.center,
                    map: map
                });
            }
        };

        $http({
            method: 'GET',
            url: nspCoreEntityPropertyService.popupUrl[$scope.dialogData.data.entityType],
            params: params
        })
        .success(function(data) {
            $scope.data = data;

            if ($scope.dialogData.data.entityType === 'product') {
                $scope.qrcode = $scope.t('Name', 'Common.Name') + ":" + $scope.data.name + $scope.t('Product Type', 'Common.ProductType') + ":" + $scope.data.ciTypeName + $scope.t('Manufacturer', 'Common.Manufacturer') + ":" + $scope.data.manufacturerName;
            } else {
                if ($scope.data.location) {
                    var longitude = $scope.data.location.longitude;
                    var latitude = $scope.data.location.latitude;
                      
                    $timeout(function(){
                        $scope.buildMap(latitude, longitude);
                    }, 0);
                }
            }
        })
        .error(function(data) {
        });
    }
})();

/****************************************
    FILE: examples/demo/demo.module.js
****************************************/

angular.module('demo', []);

/****************************************
    FILE: examples/demo/demo.routes.js
****************************************/

(function () {
    'use strict';

    config.$inject = ["$stateProvider", "$urlRouterProvider"];
    angular.module('demo').config(config);

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('/', '/app/home');
        $urlRouterProvider.when('', '/app/home');

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('app.home', {
                url: '/home',
                templateUrl: "examples/home/home.html"
            })
            .state('app.entity-properties', {
                url: '/entity-properties',
                templateUrl: "examples/entity-properties/entity-properties.html",
                controller: "EntityPropertiesController",
                controllerAs: 'vm'
            }).state('app.nsp-grid', {
                url: '/nsp-grid',
                templateUrl: "examples/nsp-grid/nsp-grid.html",
                controller: "NspGridController",
                controllerAs: 'vm'
            });
    }

})();


/****************************************
    FILE: examples/demo/demo.controller.js
****************************************/

(function() {
    'use strict';

    DemoController.$inject = ["translate", "$rootScope"];
    angular
        .module('demo')
        .controller('DemoController', DemoController);


    /* @ngInject */
    function DemoController(translate, $rootScope) {
        var vm = this;
    }
})();

/****************************************
    FILE: examples/entity-properties/entity-properties.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('demo')
        .controller('EntityPropertiesController', EntityPropertiesController);

    EntityPropertiesController.$inject = ['$scope'];

    /* @ngInject */
    function EntityPropertiesController($scope) {
        var vm = this;

        vm.title = 'Entity Properties';

        vm.data = [{
            id: 5494,
            label: 'number',
            labelKey: 'number',
            type: 'int',
            value: '2452'
        }, {
            id: 5494,
            label: 'string',
            labelKey: 'string',
            type: 'string',
            value: 'Network'
        }, {
            id: 5494,
            label: 'date time',
            labelKey: 'date time',
            type: 'datetime',
            value: '9/12/2016 12:30 AM'
        }, {
            id: 5494,
            label: 'image',
            labelKey: 'image',
            type: 'image',
            value: 'http://66.media.tumblr.com/386c139d59c149903e7cd6e110f85bb2/tumblr_n3ljsfKr221s030vgo1_1280.gif'
        }, {
            id: 5494,
            label: 'boolean',
            labelKey: 'boolean',
            type: 'boolean',
            value: false
        }, {
            entityType: 'product',
            id: "",
            label: 'Product',
            labelKey: 'Product',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: '',
            viewType: 'popup'
        }, {
            entityType: 'usedby',
            id: 5494,
            label: 'Used By',
            labelKey: 'Used By',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Ivana Kicovic',
            viewType: 'popup'
        }, {
            entityType: 'managedby',
            id: 5491,
            label: 'Managed by',
            labelKey: 'Managed by',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Sasa Matijevic',
            viewType: 'popup'
        }, {
            entityType: 'vendor',
            id: 5491,
            label: 'Vendor',
            labelKey: 'Vendor',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Milos Baltic',
            viewType: 'popup'
        }, {
            entityType: 'location',
            id: 5491,
            label: 'Location',
            labelKey: 'Location',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Podgorica, Crna Gora',
            viewType: 'popup'
        }, {
            format: null,
            label: 'Time',
            labelKey: 'Time',
            nspType: 'NSPKendoTimePickerProperty',
            type: 'time',
            value: '2016-09-11T22:30:00Z'
        }, {
            format: null,
            label: 'Date',
            labelKey: 'Date',
            nspType: 'NSPKendoDatePickerProperty',
            type: 'date',
            value: '2016-09-11T22:30:00Z'
        }, {
            format: null,
            label: 'DateTime',
            labelKey: 'DateTime',
            nspType: 'NSPKendoDateTimePickerProperty',
            type: 'date',
            value: '2016-09-11T22:30:00Z'
        }, {
            format: null,
            label: 'Text',
            labelKey: 'Text',
            nspType: 'NSPTextProperty',
            type: 'string',
            value: 'Lorem ipsum'
        }, {
            format: 'html',
            label: 'Textarea',
            labelKey: 'Textarea',
            nspType: 'NSPKendoEditorProperty',
            type: 'string',
            value: '<img src="http://www.mrwallpaper.com/wallpapers/Miami-Beach-Florida-1600x1200.jpg" alt="test">'
        }, {
            format: null,
            label: 'Number',
            labelKey: 'Number',
            nspType: 'NSPKendoNumericTextBoxProperty',
            type: 'int',
            value: 121354654321
        }, {
            format: null,
            label: 'Decimal number',
            labelKey: 'Decimal number',
            nspType: 'NSPKendoNumericTextBoxProperty',
            type: 'decimal',
            value: 123.456987
        }, {
            format: null,
            label: 'Check box',
            labelKey: 'Check box',
            nspType: 'NSPCheckBoxProperty',
            type: 'bool',
            value: 'True'
        }, {
            format: null,
            label: 'Check boxes',
            labelKey: 'Check boxes',
            nspType: 'NSPCheckBoxesProperty',
            type: 'string',
            value: '[{"TitleKey":"Option 1","Title":"Option 1","Value":true},{"TitleKey":"Option 2","Title":"Option 2","Value":true}]'
        }, {
            format: null,
            label: 'Radio button',
            labelKey: 'Radio button',
            nspType: 'NSPRadiosProperty',
            type: 'string',
            value: '[{"TitleKey":"Option 1","Title":"Option 1","Value":false},{"TitleKey":"Option 2","Title":"Option 2","Value":true}]'
        }, {
            format: null,
            label: 'Dropdown',
            labelKey: 'Dropdown',
            nspType: 'NSPKendoDropDownListProperty',
            type: 'string',
            value: 'Dropdown 1'
        }, {
            format: 'html',
            label: 'Info',
            labelKey: 'Info',
            nspType: 'NSPHelpProperty',
            type: 'string',
            value: '<strong>testera</strong> <em>test </em>test <img src="http://orig01.deviantart.net/416e/f/2013/225/f/e/gif___homer_finger_200x200_by_robbiebr-d6i1q8g.gif" alt="" />'
        }];
    }
})();

/****************************************
    FILE: examples/main.controller.js
****************************************/

(function() {
    'use strict';

    MainController.$inject = ["nspCoreService", "$rootScope", "$timeout"];
    angular
        .module('demo')
        .controller('MainController', MainController);


    /* @ngInject */
    function MainController(nspCoreService, $rootScope, $timeout) {
        var vm = this;
        vm.notificationsOptions = {};

        // $timeout(function(){
        //     $rootScope.$broadcast('notify', {
        //         type: "success",
        //         title: "Helou",
        //         content: "world"
        //     });    
        // }, 1000);
        
    }
})();

/****************************************
    FILE: examples/nsp-grid/nsp-grid.controller.js
****************************************/

(function() {
    'use strict';

    NspGridController.$inject = ["$scope"];
    angular
        .module('demo')
        .controller('NspGridController', NspGridController);

    /* @ngInject */
    function NspGridController($scope) {
        var vm = this;

        vm.grid = {
            gridId: 'testGrid',
            // searchableColumns: ["firstName"],
            gridOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: 'http://localhost:1800/CmdbPersonView/GetDataSource'
                            // data: {
                            //     param1: 'testera'
                            // }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                firstName: {
                                    type: "string",
                                    dbType: "string"
                                },
                                mail: {
                                    type: "string",
                                    dbType: "string"
                                }
                            }
                        }
                    },
                    pageSize: 5
                    // sort: [{
                    //     field:"firstName",
                    //     dir:"desc",
                    //     fixed: true
                    // }],
                    // filter: [{
                    //     field: 'firstName',
                    //     operator: 'contains',
                    //     value: 'Dejan',
                    //     fixed: true
                    // }]
                },
                columns: [{
                    field: 'firstName',
                    title: 'Name',
                    hidden: false,
                    searchable: true,
                    encoded: false
                }, {
                    field: 'mail',
                    title: 'Email',
                    hidden: false,
                    searchable: true,
                    encoded: false
                }]
            },
            searchOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: 'http://localhost:1800/CmdbPersonView/GetAutoCompleteDataSource'
                        }
                    }
                }
            },
            onReset: function(data) {
                console.log('reset');
            }
        };
    }

})();