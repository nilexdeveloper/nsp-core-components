
/****************************************
    FILE: src/app.module.js
****************************************/

angular.module('mainApp', [
    'ui.router',
    'nspCore',
    'demo',
    'kendo.directives'
]);

/****************************************
    FILE: examples/demo/demo.module.js
****************************************/

angular.module('demo', []);

/****************************************
    FILE: examples/demo/demo.routes.js
****************************************/

(function () {
    'use strict';

    angular.module('demo').config(config);

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('/', '/app/home');
        $urlRouterProvider.when('', '/app/home');

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('app.home', {
                url: '/home',
                templateUrl: "examples/home/home.html"
            })
            .state('app.entity-properties', {
                url: '/entity-properties',
                templateUrl: "examples/entity-properties/entity-properties.html",
                controller: "EntityPropertiesController",
                controllerAs: 'vm'
            }).state('app.nsp-grid', {
                url: '/nsp-grid',
                templateUrl: "examples/nsp-grid/nsp-grid.html",
                controller: "NspGridController",
                controllerAs: 'vm'
            });
    }

})();


/****************************************
    FILE: examples/demo/demo.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('demo')
        .controller('DemoController', DemoController);


    /* @ngInject */
    function DemoController(translate, $rootScope) {
        var vm = this;
    }
})();

/****************************************
    FILE: examples/entity-properties/entity-properties.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('demo')
        .controller('EntityPropertiesController', EntityPropertiesController);

    EntityPropertiesController.$inject = ['$scope'];

    /* @ngInject */
    function EntityPropertiesController($scope) {
        var vm = this;

        vm.title = 'Entity Properties';

        vm.data = [{
            id: 5494,
            label: 'number',
            labelKey: 'number',
            type: 'int',
            value: '2452'
        }, {
            id: 5494,
            label: 'string',
            labelKey: 'string',
            type: 'string',
            value: 'Network'
        }, {
            id: 5494,
            label: 'date time',
            labelKey: 'date time',
            type: 'datetime',
            value: '9/12/2016 12:30 AM'
        }, {
            id: 5494,
            label: 'image',
            labelKey: 'image',
            type: 'image',
            value: 'http://66.media.tumblr.com/386c139d59c149903e7cd6e110f85bb2/tumblr_n3ljsfKr221s030vgo1_1280.gif'
        }, {
            id: 5494,
            label: 'boolean',
            labelKey: 'boolean',
            type: 'boolean',
            value: false
        }, {
            entityType: 'product',
            id: "",
            label: 'Product',
            labelKey: 'Product',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: '',
            viewType: 'popup'
        }, {
            entityType: 'usedby',
            id: 5494,
            label: 'Used By',
            labelKey: 'Used By',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Ivana Kicovic',
            viewType: 'popup'
        }, {
            entityType: 'managedby',
            id: 5491,
            label: 'Managed by',
            labelKey: 'Managed by',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Sasa Matijevic',
            viewType: 'popup'
        }, {
            entityType: 'vendor',
            id: 5491,
            label: 'Vendor',
            labelKey: 'Vendor',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Milos Baltic',
            viewType: 'popup'
        }, {
            entityType: 'location',
            id: 5491,
            label: 'Location',
            labelKey: 'Location',
            nspType: 'NSPKendoAutoCompleteProperty',
            type: 'int',
            value: 'Podgorica, Crna Gora',
            viewType: 'popup'
        }, {
            format: null,
            label: 'Time',
            labelKey: 'Time',
            nspType: 'NSPKendoTimePickerProperty',
            type: 'time',
            value: '2016-09-11T22:30:00Z'
        }, {
            format: null,
            label: 'Date',
            labelKey: 'Date',
            nspType: 'NSPKendoDatePickerProperty',
            type: 'date',
            value: '2016-09-11T22:30:00Z'
        }, {
            format: null,
            label: 'DateTime',
            labelKey: 'DateTime',
            nspType: 'NSPKendoDateTimePickerProperty',
            type: 'date',
            value: '2016-09-11T22:30:00Z'
        }, {
            format: null,
            label: 'Text',
            labelKey: 'Text',
            nspType: 'NSPTextProperty',
            type: 'string',
            value: 'Lorem ipsum'
        }, {
            format: 'html',
            label: 'Textarea',
            labelKey: 'Textarea',
            nspType: 'NSPKendoEditorProperty',
            type: 'string',
            value: '<img src="http://www.mrwallpaper.com/wallpapers/Miami-Beach-Florida-1600x1200.jpg" alt="test">'
        }, {
            format: null,
            label: 'Number',
            labelKey: 'Number',
            nspType: 'NSPKendoNumericTextBoxProperty',
            type: 'int',
            value: 121354654321
        }, {
            format: null,
            label: 'Decimal number',
            labelKey: 'Decimal number',
            nspType: 'NSPKendoNumericTextBoxProperty',
            type: 'decimal',
            value: 123.456987
        }, {
            format: null,
            label: 'Check box',
            labelKey: 'Check box',
            nspType: 'NSPCheckBoxProperty',
            type: 'bool',
            value: 'True'
        }, {
            format: null,
            label: 'Check boxes',
            labelKey: 'Check boxes',
            nspType: 'NSPCheckBoxesProperty',
            type: 'string',
            value: '[{"TitleKey":"Option 1","Title":"Option 1","Value":true},{"TitleKey":"Option 2","Title":"Option 2","Value":true}]'
        }, {
            format: null,
            label: 'Radio button',
            labelKey: 'Radio button',
            nspType: 'NSPRadiosProperty',
            type: 'string',
            value: '[{"TitleKey":"Option 1","Title":"Option 1","Value":false},{"TitleKey":"Option 2","Title":"Option 2","Value":true}]'
        }, {
            format: null,
            label: 'Dropdown',
            labelKey: 'Dropdown',
            nspType: 'NSPKendoDropDownListProperty',
            type: 'string',
            value: 'Dropdown 1'
        }, {
            format: 'html',
            label: 'Info',
            labelKey: 'Info',
            nspType: 'NSPHelpProperty',
            type: 'string',
            value: '<strong>testera</strong> <em>test </em>test <img src="http://orig01.deviantart.net/416e/f/2013/225/f/e/gif___homer_finger_200x200_by_robbiebr-d6i1q8g.gif" alt="" />'
        }];
    }
})();

/****************************************
    FILE: examples/main.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('demo')
        .controller('MainController', MainController);


    /* @ngInject */
    function MainController(nspCoreService, $rootScope, $timeout) {
        var vm = this;
        vm.notificationsOptions = {};

        // $timeout(function(){
        //     $rootScope.$broadcast('notify', {
        //         type: "success",
        //         title: "Helou",
        //         content: "world"
        //     });    
        // }, 1000);
        
    }
})();

/****************************************
    FILE: examples/nsp-grid/nsp-grid.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('demo')
        .controller('NspGridController', NspGridController);

    /* @ngInject */
    function NspGridController($scope) {
        var vm = this;

        vm.grid = {
            gridId: 'testGrid',
            // searchableColumns: ["firstName"],
            gridOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: 'http://localhost:1800/CmdbPersonView/GetDataSource'
                            // data: {
                            //     param1: 'testera'
                            // }
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                firstName: {
                                    type: "string",
                                    dbType: "string"
                                },
                                mail: {
                                    type: "string",
                                    dbType: "string"
                                }
                            }
                        }
                    },
                    pageSize: 5
                    // sort: [{
                    //     field:"firstName",
                    //     dir:"desc",
                    //     fixed: true
                    // }],
                    // filter: [{
                    //     field: 'firstName',
                    //     operator: 'contains',
                    //     value: 'Dejan',
                    //     fixed: true
                    // }]
                },
                columns: [{
                    field: 'firstName',
                    title: 'Name',
                    hidden: false,
                    searchable: true,
                    encoded: false
                }, {
                    field: 'mail',
                    title: 'Email',
                    hidden: false,
                    searchable: true,
                    encoded: false
                }]
            },
            searchOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: 'http://localhost:1800/CmdbPersonView/GetAutoCompleteDataSource'
                        }
                    }
                }
            },
            onReset: function(data) {
                console.log('reset');
            }
        };
    }

})();