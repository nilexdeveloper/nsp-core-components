angular.module('formEngine').directive('nspTheme', ['$http', 'UtilityService', 'themeService', function ($http, UtilityService, themeService) {
  return {
    restrict: 'A',
    scope: {
      options: '=nspTheme'
    },
    link: function (scope, element, attrs) {

      // Do nothing if no options are provided.
      if (!scope.options || scope.options.length === 0) {
        return false;
      }

      // Keys are CSS property names and values are properties from themeService object.
      var options = scope.options;

      // Replace values with actual values from themeService.
      for (var property in options) {

        // Remove property if it doesn't exist in themeService to prevent CSS errors.
        if (themeService[options[property]]) {
          options[property] = themeService[options[property]];
        } else {
          NSP.Log.warning('Theme doesn\'t support property: ' + options[property]);
          delete options[property];
        }
      }

      // Attach styles to element.
      $(element).css(options);
    }
  };
}]);