module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                sourceMap: true,
                outputStyle: "nested" // TODO: "compresed" doesn't work in Safari
            },
            dist: {
                files: {
                    'dist/css/main.css': 'src/main.scss',
                    'dist/css/entity-property.css': 'src/components/entity-properties/web/entity-properties.scss'
                }
            }
        },

        concat: {

            options: {
                process: function(src, filepath) {
                    return '\n/****************************************\n    FILE: ' + filepath + '\n****************************************/\n\n' + src;
                }
            },

            app: {
                src: [
                    'src/nsp-core.module.js',
                    'src/nsp-core.service.js',
                    'src/nsp-core.templates.js',
                    'src/components/**/*.module.js',
                    'src/components/**/*.config.js',
                    'src/components/**/*.routes.js',
                    'src/components/**/*.filter.js',
                    'src/components/**/*.service.js',
                    'src/components/**/*.directive.js',
                    'src/components/**/*.controller.js',

                    'examples/**/*.module.js',
                    'examples/**/*.config.js',
                    'examples/**/*.routes.js',
                    'examples/**/*.filter.js',
                    'examples/**/*.service.js',
                    'examples/**/*.directive.js',
                    'examples/**/*.controller.js'
                ],
                dest: 'dist/js/script.js',
                nonull: true
            },

            examples: {
                src: [
                    'src/app.module.js',
                    'examples/**/*.module.js',
                    'examples/**/*.config.js',
                    'examples/**/*.routes.js',
                    'examples/**/*.filter.js',
                    'examples/**/*.service.js',
                    'examples/**/*.directive.js',
                    'examples/**/*.controller.js'
                ],
                dest: 'dist/js/examples.js',
                nonull: true
            }

        },

        wiredep: {
            task: {
                src: ['index.html']
            },
            options: {
                devDependencies: true
            }
        },

        includeSource: {
            options: {
                basePath: '',
                baseUrl: '',
                templates: {
                    html: {
                        js: '<script src="{filePath}"></script>',
                        css: '<link rel="stylesheet" type="text/css" href="{filePath}" />'
                    }
                }
            },
            dev: {
                files: {
                    'index.html': 'index.html'
                }
            }
        },

        ngtemplates: {
            nspCore: {
                src: '**/**.html',
                dest: 'src/nsp-core.templates.js',
                cwd: 'src/components',
                options: {
                    htmlmin: {
                        collapseWhitespace: true
                    },
                    prefix: '/'
                }
            }
        },

        ngAnnotate: {
            app: {
                files: {
                    '<%= concat.app.dest %>': ['<%= concat.app.dest %>']
                }
            }
        },

        jshint: {
            all: {
                src: [
                    'src/**/*.js'
                ]
            },
            options: {
                notypeof: true,
                debug: true,
                eqnull: true,
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },

        watch: {
            all: {
                files: ['Gruntfile.js',
                    'src/**/*.js',
                    'src/**/*.scss',
                    'src/**/*.html',
                    '!src/nsp-core.templates.js',

                    'examples/**/*.js',
                    'examples/**/*.scss',
                    'examples/**/*.html'
                ],
                tasks: ['default'],
                options: {
                    atBegin: true,
                    livereload: false
                }
            }
        },

        connect: {
            server: {
                options: {
                    livereload: false,
                    keepalive: true,
                    port: 8000,
                    hostname: '*'
                }
            }
        },

        clean: {
            dist: {
                src: ["dist/**/*"],
                filter: function(filepath) {
                    if (!grunt.file.isDir(filepath)) {
                        return true;
                    }
                    return (fs.readdirSync(filepath).length === 0);
                }
            }
        }

    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['clean', 'concat', 'sass', 'newer:jshint', 'ngtemplates', 'includeSource', 'wiredep', 'ngAnnotate']);

};