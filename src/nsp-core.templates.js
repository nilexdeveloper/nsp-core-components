angular.module('nspCore').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('/comments/nsp-comment-attachment-list.html',
    "<ul class=\"mailing-form\"><li ng-repeat=\"attachment in items\" title=\"{{attachment.fileName}}\"><a href=\"#\" ng-click=\"downloadAttachment(attachment)\"><img ng-src=\"{{attachmentImageSrc(attachment)}}\" alt=\"{{attachment.fileName}}\"></a></li></ul>"
  );


  $templateCache.put('/comments/nsp-comment-preview.html',
    "<div kendo-window-content><div ng-bind-html=\"dialogData.message\"></div></div>"
  );


  $templateCache.put('/comments/nsp-comment-timer.html',
    "<!-- TODO: Localization --><div ng-form name=\"timerForm\"><label>{{t('Timer', 'Common.Timer')}}:</label><button ng-click=\"toggle($event)\" class=\"btn btn-default\">{{toggleBtnText}}</button> <input name=\"timerInput\" type=\"text\" placeholder=\"hh:mm\" ng-model=\"timerInput\" ng-pattern=\"timerPattern\" ng-change=\"inputChanged()\" ng-focus=\"timerInputFocus()\" ng-blur=\"timerInputBlur()\"></div>"
  );


  $templateCache.put('/comments/nsp-comments-form.html',
    "<div class=\"comments-form\"><form sf-schema=\"schema\" sf-form=\"form\" sf-model=\"model\" name=\"{{formId}}\" id=\"{{formId}}\" ng-submit=\"onCommentSubmit()\" autocomplete=\"false\" class=\"form-engine\"></form><div class=\"form-actions\"><div file-drop file-drop-options=\"settings.uploadAttachmentsOptions\" class=\"file-drop\" ng-model=\"newComment.attachments\" ng-if=\"settings.uploadAttachmentsOptions\"></div><button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('public')\" ng-class=\"{active: newComment.isPublic}\" title=\"{{t('Change to public comment', 'PublicReply')}}\" ng-if=\"settings.publicMark\"><i class=\"icon-users\"></i></button> <button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('private')\" ng-class=\"{active: newComment.isPrivate}\" title=\"{{t('Change to internal comment','InternalNote')}}\" ng-if=\"settings.privateMark\"><i class=\"icon-user\"></i></button> <button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('solution')\" ng-class=\"{active: newComment.isSolution}\" ng-if=\"settings.solutionMark\" title=\"{{t('Mark as a Solution', 'MarkAsASolution')}}\"><i class=\"icon-lamp\"></i></button> <button class=\"btn btn-default comment-mark\" ng-click=\"newCommentMark('workaround')\" ng-class=\"{active: newComment.isWorkaround}\" ng-if=\"settings.workaroundMark\" title=\"{{t('Mark as a Workaround', 'MarkAsAWorkaround')}}\"><i class=\"icon-loop\"></i></button> <span class=\"separator\" ng-hide=\"!settings.privateMark && !settings.publicMark\">|</span> <button class=\"btn btn-default\" ng-click=\"onCommentSubmit()\">{{t('Save comment', 'Common.SaveComment')}}</button> <span class=\"separator\">|</span> <span class=\"comment-mark-info\" ng-if=\"newComment.isPublic\" ng-if=\"settings.publicMark\">{{t(\"Your comment will be sent as a public reply\", \"PublicReplyCommentDescription\")}} </span><span class=\"comment-mark-info\" ng-if=\"newComment.isPrivate\" ng-if=\"settings.privateMark\">{{t(\"Your comment will be sent as an internal note\", \"InternalReplyCommentDescription\")}} </span><span class=\"separator\">|</span> <span class=\"comment-mark-info\" ng-if=\"newComment.isSolution\" ng-if=\"settings.solutionMark\">{{t(\"Your comment will be marked as solution\", \"SolutionReplyCommentDescription\")}} </span><span class=\"comment-mark-info\" ng-if=\"newComment.isWorkaround\" ng-if=\"settings.workaroundMark\">{{t(\"Your comment will be marked as workaround\", \"WorkaroundReplyCommentDescription\")}}</span></div></div>"
  );


  $templateCache.put('/comments/nsp-comments-list.html',
    "<div class=\"comments-list\" ng-if=\"commentsList.length\"><div class=\"heading\"><div class=\"comment-filters\"><button class=\"btn btn-default comment-mark\" title=\"{{t('Show solution comments only', 'ShowSolutionCommentsOnly')}}\" ng-class=\"{active: activeMarkFilter === 'solution'}\" ng-click=\"markFilter('solution')\" ng-if=\"countComments('solution')\"><i class=\"icon-lamp\"></i> <span class=\"count\">{{countComments('solution')}}</span></button> <button class=\"btn btn-default comment-mark\" title=\"{{t('Show workaround comments only', 'ShowWorkaroundCommentsOnly')}}\" ng-click=\"markFilter('workaround')\" ng-class=\"{active: activeMarkFilter === 'workaround'}\" ng-if=\"countComments('workaround')\"><i class=\"icon-loop\"></i> <span class=\"count\">{{countComments('workaround')}}</span></button> <button class=\"btn btn-default comment-mark\" title=\"{{t('Show attachment comments only', 'ShowAttachmentCommentsOnly')}}\" ng-click=\"markFilter('attachments')\" ng-class=\"{active: activeMarkFilter === 'attachments'}\" ng-if=\"countComments('attachments')\"><i class=\"icon-attach\"></i> <span class=\"count\">{{countComments('attachments')}}</span></button> <button class=\"btn btn-default comment-mark\" title=\"{{t('Comments', 'Common.Comments')}}\" ng-click=\"markFilter('all')\" ng-class=\"{active: activeMarkFilter === 'all'}\"><i class=\"icon-comment\"></i> <span class=\"count\">{{countComments('all')}}</span></button></div><div class=\"comments-search\"><input class=\"comments-search\" type=\"text\" ng-model=\"searchValue\" placeholder=\"{{t('Search comment', 'SearchComment')}}...\"> <button class=\"btn\"><i class=\"k-icon k-i-search\"></i></button></div><button class=\"btn collapse-all\" ng-click=\"collapseToggleAll()\"><i ng-class=\"{'icon-plus-squared': allCollapsed, 'icon-minus-squared': !allCollapsed}\"></i> {{allCollapsed ? t('Expand all', 'ExpandAll') : t('Collapse all', 'CollapseAll')}}</button></div><ul class=\"list\"><li ng-repeat=\"item in commentsList | filterComments:searchValue:activeMarkFilter\" ng-class=\"{'marked-as-public':item.isPublic, 'marked-as-private':item.isPrivate, 'marked-as-solution':item.isSolution, 'marked-as-workaround':item.isWorkaround, 'collapsed':item.collapsed}\"><div class=\"aside\"><img ng-src=\"{{item.avatar}}\" alt=\"User image\" ng-if=\"item.avatar\" class=\"avatar\"> <span class=\"collapser\" ng-click=\"collapseToggle(item.id)\" ng-class=\"{'f-nsp f-nsp-angle-down':!item.collapsed, 'f-nsp f-nsp-angle-right':item.collapsed}\"></span></div><div class=\"inner\"><div class=\"heading\"><div class=\"comment-actions\"><button class=\"btn\" ng-if=\"settings.sendMail\" ng-click=\"sendEmail(item)\" title=\"{{t('SendMail','Common.SendEmail')}}\"><i class=\"icon-mail\"></i></button> <button class=\"btn\" ng-if=\"settings.solutionMark\" ng-click=\"mapArticle(item)\" title=\"{{t('CreateArticle','Common.CreateArticle')}}\"><i class=\"f-nsp f-nsp-knowledge-base\"></i></button> <button class=\"btn btn-like\" ng-click=\"likeComment(item.id)\" ng-class=\"{'active': item.isLiked}\"><i class=\"f-nsp f-nsp-support\"></i> <span class=\"count\">{{item.likes}}</span></button> <button class=\"btn\" ng-if=\"settings.solutionMark && !item.isSolution && !item.isWorkaround\" ng-click=\"changeCommentMark(item.id, 'solution')\" title=\"{{t('Mark as a Solution', 'MarkAsASolution')}}\"><i class=\"icon-lamp\"></i></button> <button class=\"btn\" ng-if=\"settings.workaroundMark && !item.isSolution && !item.isWorkaround\" ng-click=\"changeCommentMark(item.id, 'workaround')\" title=\"{{t('Mark as a Workaround', 'MarkAsAWorkaround')}}\"><i class=\"icon-loop\"></i></button> <button class=\"btn\" ng-if=\"settings.solutionMark && item.isSolution\" ng-click=\"changeCommentMark(item.id, 'solution')\" title=\"{{t('Not a Solution', 'NotASolution')}}\"><i class=\"icon-block\"></i></button> <button class=\"btn\" ng-if=\"settings.workaroundMark && item.isWorkaround\" ng-click=\"changeCommentMark(item.id, 'workaround')\" title=\"{{t('Not a Workaround', 'NotAWorkaround')}}\"><i class=\"icon-block\"></i></button> <button class=\"btn\" ng-if=\"settings.publicMark && item.isPrivate\" ng-click=\"changeCommentMark(item.id, 'public')\" title=\"{{t('Change to public comment', 'PublicReply')}}\"><i class=\"icon-users\"></i></button> <button class=\"btn\" ng-if=\"settings.privateMark && item.isPublic\" ng-click=\"changeCommentMark(item.id, 'private')\" title=\"{{t('Change to internal comment','InternalNote')}}\"><i class=\"icon-user\"></i></button> <span class=\"date\">{{timeAgo(item.createdDate, 'g')}}</span></div><div class=\"author\"><span class=\"source-icon\" ng-class=\"item.sourceIcon\"></span> {{item.author}} <span class=\"time-spent\" ng-if=\"settings.timeSpent !== false\">({{t('Time spent', 'Common.TimeSpent')}}: {{formatTimeSpent(item.timeSpent)}})</span></div></div><div class=\"comment\" ng-bind-html=\"item.comment\" text-pane-popup-images></div><ul class=\"attachments\" ng-if=\"item.attachments\"><li ng-repeat=\"attachment in item.attachments\" title=\"{{attachment.fileName}}\"><a href=\"#\" ng-click=\"downloadAttachment(attachment)\"><img ng-src=\"{{attachmentImageSrc(attachment)}}\" alt=\"{{attachment.fileName}}\"></a></li></ul></div></li></ul></div>"
  );


  $templateCache.put('/comments/nsp-send-email-comment.html',
    "<div ng-controller=\"NspSendEmailCommentController\" kendo-window-content><form sf-schema=\"schema\" sf-form=\"form\" sf-model=\"model\" name=\"{{formId}}\" id=\"{{formId}}\" ng-submit=\"onSubmit()\" autocomplete=\"false\" class=\"form-engine add-edit-window\"></form><div nsp-comment-attachments-list comment-attachments-list-items=\"dialogData.comment.attachments\" ng-if=\"dialogData.comment.attachments\"></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-location-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view location-view\"><div class=\"map\" ng-class=\"{'no-location': !data.location}\"><div ng-if=\"!data.location\" class=\"no-location-info\"><p>{{t('No', 'Common.No')}}<br>{{t('Location', 'Common.Location')}}<br>{{t('Found', 'Common.Found')}}</p></div><div class=\"map-canvas\" id=\"map-canvas\"></div><div class=\"address-details\"><p class=\"list-title\"><b>{{t('Address', 'Common.Address')}}:</b></p><ul><li ng-if=\"data.address\">{{data.address}}</li><li ng-if=\"data.city\">{{data.city}}</li><li ng-if=\"data.zip\">{{data.zip}}</li><li ng-if=\"data.country\">{{data.country}}</li><li ng-if=\"data.countryCode\">{{data.countryCode}}</li></ul></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-product-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view product-view\"><div class=\"basic\"><p><b>{{data.name}}</b></p><div ng-bind-html=\"trustHtml(data.description) || '-'\"></div></div><div class=\"additional-info\"><div class=\"product-details\"><table><tr ng-if=\"data.ciTypeName\"><td>{{t('CI Type', 'Common.CIType')}}</td><td>{{data.ciTypeName}}</td></tr><tr ng-if=\"data.manufacturerName\"><td>{{t('Manufacturer', 'Common.Manufacturer')}}</td><td>{{data.manufacturerName}}</td></tr><tr ng-if=\"data.depreciationTypeName\"><td>{{t('Depreciation Model', 'Common.DepreciationModel')}}</td><td>{{data.depreciationTypeName}}</td></tr></table></div><div class=\"code\"><div ng-if=\"qrcode\" class=\"qr-code\"><span kendo-qrcode k-error-correction=\"'M'\" ng-model=\"qrcode\" k-size=\"90\"></span></div><div ng-if=\"data.codeString && data.codeType\" class=\"barcode\"><span kendo-barcode k-type=\"data.codeType\" ng-model=\"data.codeString\" k-height=\"60\" k-width=\"130\"></span></div></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-user-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view user-view\"><div class=\"info\"><div class=\"basic\"><p><b>{{data.firstName}} {{data.lastName}}</b></p><p><b><i>{{data.personTitle}}</i></b></p></div><div class=\"details\"><ul class=\"contact-details\"><li ng-if=\"data.mail\">{{data.mail}}</li><li ng-if=\"data.phone\">{{data.phone}}</li></ul><ul class=\"department-details\"><li ng-if=\"data.department\">{{data.department}}</li><li ng-if=\"data.jobTitle\">{{data.jobTitle}}</li><li ng-if=\"data.birthDate\">{{data.birthDate}}</li></ul></div></div><div class=\"map\" ng-class=\"{'no-location': !data.location}\"><div ng-if=\"!data.location\" class=\"no-location-info\"><p>{{t('No', 'Common.No')}}<br>{{t('Location', 'Common.Location')}}<br>{{t('Found', 'Common.Found')}}</p></div><div class=\"map-canvas\" id=\"map-canvas\"></div><div class=\"address-details\" ng-if=\"data.address || data.city || data.zip || data.country || data.countyCode\"><p class=\"list-title\"><b>{{t('Address', 'Common.Address')}}:</b></p><ul><li ng-if=\"data.address\">{{data.address}}</li><li ng-if=\"data.city\">{{data.city}}</li><li ng-if=\"data.zip\">{{data.zip}}</li><li ng-if=\"data.country\">{{data.country}}</li><li ng-if=\"data.countryCode\">{{data.countryCode}}</li></ul></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/entity-property-vendor-popup.html',
    "<div ng-controller=\"EntityPropertyPopupController\"><div ng-if=\"data\" class=\"view vendor-view\"><div class=\"info\"><div class=\"basic\"><p><b>{{data.name}}</b></p><div ng-bind-html=\"trustHtml(data.description) || '-'\"></div></div><div class=\"details\"><p><b>{{t('Contact', 'Common.Contact')}}:</b></p><ul><li ng-if=\"data.contactPerson\"><b>{{data.contactPerson}}</b></li><li ng-if=\"data.phone\">{{data.phone}}</li><li ng-if=\"data.fax\">{{data.fax}}</li><li ng-if=\"data.mobile\">{{data.mobile}}</li><li ng-if=\"data.mail\">{{data.mail}}</li></ul></div></div><div class=\"map\" ng-class=\"{'no-location':!data.location}\"><div ng-if=\"!data.location\" class=\"no-location-info\"><p>{{t('No', 'Common.No')}}<br>{{t('Location', 'Common.Location')}}<br>{{t('Found', 'Common.Found')}}</p></div><div class=\"map-canvas\" id=\"map-canvas\"></div><div class=\"address-details\"><p class=\"list-title\"><b>{{t('Address', 'Common.Address')}}:</b></p><ul><li ng-if=\"data.address\">{{data.address}}</li><li ng-if=\"data.city\">{{data.city}}</li><li ng-if=\"data.zip\">{{data.zip}}</li><li ng-if=\"data.country\">{{data.country}}</li><li ng-if=\"data.countryCode\">{{data.countryCode}}</li></ul></div></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/check-box.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div class=\"value\"><div ng-if=\"options.value\" class=\"check-box-checked\"></div><div ng-if=\"!options.value\" class=\"check-box-unchecked\"></div><i class=\"f-nsp\" ng-class=\"{'f-nsp-checked': options.value, 'f-nsp-unchecked': !options.value}\"></i> <span class=\"option-label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/check-boxes.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><div ng-repeat=\"item in options.value\"><div ng-if=\"item.Value\" class=\"check-box-checked\"></div><div ng-if=\"!item.Value\" class=\"check-box-unchecked\"></div><i class=\"f-nsp\" ng-class=\"{'f-nsp-checked': item.Value, 'f-nsp-unchecked': !item.Value}\"></i> <span>{{t(item.Title, item.TitleKey)}}</span></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/image.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\" nsp-core-text-pane-popup-images><img ng-if=\"options.value\" ng-src=\"{{options.value}}\" alt=\"{{options.label}}\"> <span ng-if=\"!options.value\">-</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/popup.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><span ng-click=\"openPopup()\" ng-class=\"{'link': options.id}\">{{options.value ? options.value : '-'}}</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/radio-button.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><div ng-repeat=\"item in options.value track by $index\"><div ng-if=\"item.Value\" class=\"radio-button-checked\"></div><div ng-if=\"!item.Value\" class=\"radio-button-unchecked\"></div><i class=\"f-nsp\" ng-class=\"{'f-nsp-radio': item.Value, 'f-nsp-circle-o': !item.Value}\"></i> <span>{{t(item.Title, item.TitleKey)}}</span></div></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/text.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\"><span>{{options.value ? options.value : '-'}}</span></div></div>"
  );


  $templateCache.put('/entity-properties/web/templates/textarea.html',
    "<div class=\"entity-property ep-type-{{propertyType}}\"><div ng-if=\"options.label\" class=\"label\">{{options.labelKey ? t(options.label, options.labelKey) : options.label}}</div><div class=\"value\" ng-bind-html=\"trustHtml(options.value) || '-'\" nsp-core-text-pane-popup-images></div></div>"
  );


  $templateCache.put('/kendo-window/delete-window.html',
    "<div class=\"dynamic-grid-delete-window\" delete-window><p>{{message}}</p><button class=\"btn btn-primary\" ng-click=\"delete()\">{{confirmButtonText}}</button> <button class=\"btn btn-primary\" ng-click=\"cancel()\">{{cancelButtonText}}</button></div>"
  );


  $templateCache.put('/notification/notification.html',
    "<div class=\"nsp-notification\"><div class=\"notification-container\" ng-style=\"{'top': settings.kendoOptions.position.top, 'right': settings.kendoOptions.position.right, 'bottom': settings.kendoOptions.position.bottom, 'left': settings.kendoOptions.position.left}\"></div><span kendo-notification=\"widget\" k-options=\"settings.kendoOptions\"></span></div>"
  );


  $templateCache.put('/notification/templates/default.html',
    "<span class=\"close f-nsp f-nsp-remove\" ng-click=\"close($event)\"></span><h3 class=\"title\" ng-if=\"dataItem.title\">{{dataItem.title}}</h3><div class=\"content\">{{dataItem.content}}</div>"
  );


  $templateCache.put('/nsp-grid/templates/nsp-grid.template.html',
    "<div class=\"toolbar\"><div class=\"search\"><input class=\"search-input\" id=\"{{settings.gridId + 'Search'}}\" kendo-auto-complete=\"searchWidget\" k-options=\"settings.searchOptions\" ng-model=\"settings.searchText\" ng-keydown=\"$event.which === 13 && searchGrid()\" ng-keyup=\"$event.which === 8 && settings.searchText == '' && clearSearch()\"> <a class=\"k-icon k-i-search\" ng-click=\"searchGrid()\"></a></div><button type=\"button\" class=\"refresh-btn\" ng-click=\"refreshGrid()\" title=\"{{t('Refresh', 'Common.Refresh')}}\"><i class=\"f-nsp f-nsp-refresh\"></i></button> <button type=\"button\" class=\"reset-filters-btn toolbar-btn-transparent\" ng-click=\"reset()\" ng-if=\"showReset\">{{t('Reset all filters', 'Common.ResetAllFilters')}}</button></div><div kendo-grid=\"gridWidget\" k-options=\"settings.gridOptions\" id=\"{{settings.gridId}}\" class=\"grid-container\"></div>"
  );

}]);
