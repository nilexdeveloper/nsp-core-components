﻿angular.module('nspCore').factory('nspCommentsService', ['$http', '$q', 'translate', '$templateCache', 'kendoWindowService', 'appData', function($http, $q, translate, $templateCache, kendoWindowService, appData) {
    var factory = {};

    factory.commentAction = function(params, url) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: url,
            dataType: 'json',
            data: params,
            header: "application/json"
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (error, status) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    factory.getCommentTemplate = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: "/SimpleTicketView/GetSelectedTemplateData",
            data: params,
            header: "application/json"
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (error, status) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    factory.getTagValue = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: "/SimpleTicketView/AppendTicketTag",
            data: params,
            header: "application/json"
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (error, status) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    // Kendo Editor Options
    factory.editorOptions = {
        resizable: false,
        tools: [{
            name: "bold",
            tooltip: translate('Bold', 'Bold')
        }, {
            name: "italic",
            tooltip: translate('Italic', 'Italic')
        }, {
            name: "underline",
            tooltip: translate('Underline', 'Underline')
        }, {
            name: "strikethrough",
            tooltip: translate('Strikethrough', 'Strikethrough')
        }, {
            name: "insertUnorderedList",
            tooltip: translate('InsertUnorderedList', 'Insert unordered list')
        }, {
            name: "insertOrderedList",
            tooltip: translate('InsertOrderedList', 'Insert ordered list')
        }, {
            name: "createLink",
            tooltip: translate('CreateLink', 'Insert hyperlink')
        }, {
            name: "unlink",
            tooltip: translate('Unlink', 'Unlink')
        }, {
            name: "fontName",
            tooltip: translate('FontName', 'Font name')
        }, {
            name: "fontSize",
            items: [{
                text: '11px',
                value: '11px'
            }, {
                text: '13px',
                value: '13px'
            }, {
                text: '16px',
                value: '16px'
            }, {
                text: '19px',
                value: '19px'
            }, {
                text: '24px',
                value: '24px'
            }, {
                text: '32px',
                value: '32px'
            }, {
                text: '48px',
                value: '48px'
            }],
            tooltip: translate('FontSize', 'Font size')
        }, {
            name: "preview",
            template: '<a href="" role="button" class="k-tool preview" unselectable="on"><span unselectable="on" class="k-preview">' + translate('Preview', 'Common.Preview') + '</span></a>',
            exec: function(event) {
                var editor = $(this).data('kendoEditor'),
                    editorScope = $(editor.element[0]).scope(),
                    value = editor.value();

                var kendoWindowOptions = {
                    title: translate("Comment Preview", "Common.CommentPreview"),
                    modal: true,
                    width: 600,
                    maxHeight: 600,
                    content: {
                        template: $templateCache.get('/comments/nsp-comment-preview.html')
                    }
                };

                var windowData = {
                    message: value
                };

                if (value) {
                    kendoWindowService.create('commentPreview', windowData, kendoWindowOptions);
                } else {
                    editorScope.ngModel.$setValidity('tv4-302', false);
                    editorScope.$broadcast('schemaFormValidate');
                }   

            }
        }, {
            name: "insertTags",
            template: '<div insert-comment-tag class="insert-comment-tag"></div>'
        }, {
            name: "selectTemplate",
            template: '<div select-comment-template class="select-comment-template"></div>'
        }]
    };

    factory.enableTimer = appData.person ? appData.person.enableTimer : false;
    factory.enableAutoTimer = appData.person ? appData.person.enableAutoTimer : false;

    if (factory.enableTimer) {
        factory.editorOptions.tools.push({
            name: "timer",
            template: '<span nsp-comment-timer class="comment-timer"></span>'
        });
        if (factory.enableAutoTimer) {
            factory.editorOptions.keyup = function(event) {
                var editorScope = $(event.sender.element[0]).scope(),
                    value = this.value();
                if (value) {
                    editorScope.$broadcast('autoTimerStart');
                }
            };
        }
    }

    return factory;
}]);