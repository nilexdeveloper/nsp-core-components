﻿angular.module('nspCore').directive('nspCommentAttachmentsList', ['$http', 'translate', 'appData', 'nspCommentsService', '$filter', 'kendoWindowService', function ($http, translate, appData, nspCommentsService, $filter, kendoWindowService) {
    return {
        scope: {
            items: "=commentAttachmentsListItems"
        },
        restrict: 'AE',
        replace: true,
        templateUrl: '/comments/nsp-comment-attachment-list.html',
        link: function (scope, element, attrs) {

            scope.attachmentImageSrc = function (attachment) {
                var imageSourceIcon = attachment.imageSourceIcon,
                    src = "";
                if (imageSourceIcon) {
                    src = imageSourceIcon;
                } else {
                    src = '/Components/GetImageThumbnailById?imageId=' + attachment.blobFileStorageId;
                }
                return src;
            };

            scope.downloadAttachment = function (attachment) {
                var url = "/SelfServicePortal/DownloadFile?blobFileStorageId=" + attachment.blobFileStorageId + "&downloadParent=true";
                window.location = url;
            };

        }
    };
}]);