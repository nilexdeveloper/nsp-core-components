﻿angular.module('nspCore')
  .controller('NspSendEmailCommentController', ['$scope', '$http', 'translate', 'themeService', '$compile', '$timeout', 'UtilityService', 'kendoWindowService', '$templateCache', '$sce',
    function ($scope, $http, translate, themeService, $compile, $timeout, UtilityService, kendoWindowService, $templateCache, $sce) {

        var dialogId = 'commentSendAsEmail',
          dialog = $('#' + dialogId).data('kendoWindow'),
          dialogData = kendoWindowService.read(dialogId),
          newToEmailitemtext;

        $scope.formId = "SendEmailCommentForm";

        $scope.t = function (text, key) {
            return translate(text, key);
        };

        $scope.schema = {
            "type": "object",
            "properties": {
                "recipentMailArray": {
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                },
                "sendEmailCommentFormSubject": {
                    "type": "string"
                },
                "messageContent": {
                    "type": "string"
                }
            }
        };

        $scope.form = [{
            "key": "recipentMailArray",
            "type": "kendo-multi-select",
            "title": "Recipents",
            "titleKey": "Common.Recipents",
            "required": true,
            "kendoOptions": {
                filter: 'contains',
                ignoreCase: false,
                autoBind: false,
                dataSource: {
                    type: 'jsonp',
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "/TabNavigation/GetDropDownDataOfBaseMultiSelect?instanceId=0&entityTypeId=0&dataObject=BaseEndUser&userIds=0"
                        },
                        parameterMap: function (options) {
                            return options;
                        }
                    }
                },
                dataBound: function () {
                    if ((newToEmailitemtext || this._prev) && newToEmailitemtext != this._prev) {
                        newToEmailitemtext = this._prev;

                        if (newToEmailitemtext.length > 0) {
                            if (isValidEmailAddress(newToEmailitemtext)) {
                                this.dataSource.options.serverFiltering = false;
                                this.dataSource.add({ name: newToEmailitemtext + " ", value: newToEmailitemtext, email: newToEmailitemtext });
                                this.refresh();
                                this.search();
                                this.open();
                            } else {
                                this.dataSource.options.serverFiltering = true;
                            }
                        }
                    }
                },
                dataTextField: 'name',
                dataValueField: 'value',
                dataAdditionalFields: ["email"],
                animation: false
            }
        }, {
            "key": "sendEmailCommentFormSubject",
            "type": "default",
            "title": "Subject",
            "titleKey": "Common.Subject",
            "required": true
        }, {
            "key": "messageContent",
            "type": "kendo-editor",
            "title": "Your message",
            "titleKey": "Common.YourMessage",
            "required": true,
            "kendoOptions": {
                "encoded": true
            }
        }, {
            "type": "actions",
            "items": [{
                "type": "submit",
                "title": "Done",
                "titleKey": "Done"
            }, {
                "key": "cancelButton",
                "type": "button",
                "title": "Cancel",
                "titleKey": "Common.Cancel",
                "onClick": "cancel()"
            }]
        }];

        var attachmetIds = '';
        for (var i = 0; i < dialogData.comment.attachments.length; i++) {
            if (attachmetIds === '') {
                attachmetIds = dialogData.comment.attachments[i].blobFileStorageId.toString();
            } else {
                attachmetIds = attachmetIds + ',' + dialogData.comment.attachments[i].blobFileStorageId.toString();
            }
        }


        $scope.model = {
            "referenceNumber": dialogData.additionalData.referenceNumber,
            "subject": dialogData.additionalData.subject,
            "sendEmailCommentFormSubject": dialogData.additionalData.referenceNumber + ' ' + dialogData.additionalData.subject,
            "recipentMailArray": "",
            "messageContent": $sce.getTrustedHtml(dialogData.comment.comment),
            "entityInstanceId": dialogData.additionalData.entityInstanceId,
            "entityTypeId": dialogData.additionalData.entityTypeId,
            "attachments": attachmetIds,

        };

        /*
         * Form Submit Event
         * @param  {object} form Form fields
         */
        $scope.onSubmit = function () {
            var form = $scope[$scope.formId];
            $scope.$broadcast('schemaFormValidate');

            if (form.$valid) {
                $scope.submitForm();
            } else {
                if (window.nspSendCommonNotification.error) {
                    window.nspSendCommonNotification.error({
                        notificationHeader: '',
                        notificationMessage: $scope.t('Form validation failed', 'ErrorFormValidation')
                    });
                }
            }
        };

        /*
         * Submits form to server
         * @return {[type]} [description]
         */
        $scope.submitForm = function () {
            var emails = [];
            for (var i = 0; i < $scope.model.recipentMailArray.length; i++) {
                emails.push($scope.model.recipentMailArray[i].email);
            }

            var params = {
                CommentData: $scope.model.messageContent,
                EntityInstanceId: $scope.model.entityInstanceId,
                EntityTypeId: $scope.model.entityTypeId,
                ToEmailDirect: emails,
                Subject: $scope.model.subject,
                RefrenceNo: $scope.model.referenceNumber,
                FileAttachmentListIds: $scope.model.attachments,

            };

            UtilityService.Loader.Transparent.show();

            $http({
                method: 'POST',
                url: dialogData.additionalData.urlSendEmail,
                data: params, // Pass form model
                headers: { 'Content-Type': 'application/json' }
            })
            .success(function (response) {
                UtilityService.Loader.Transparent.hide();
                if (response.success === true) {
                    if (window.nspSendCommonNotification.success) {
                        window.nspSendCommonNotification.success({
                            notificationHeader: '',
                            notificationMessage: $scope.t('Email sent', 'Common.EmailSentSuccess')
                        });
                    }
                    dialog.close();
                } else {
                    if (window.nspSendCommonNotification.error) {
                        window.nspSendCommonNotification.error({
                            notificationHeader: '',
                            notificationMessage: $scope.t('Email was not sent', 'Common.EmailSentError')
                        });
                    }
                }
            })
            .error(function (response) {
                UtilityService.Loader.Transparent.hide();
                if (window.nspSendCommonNotification.error) {
                    window.nspSendCommonNotification.error({
                        notificationHeader: '',
                        notificationMessage: $scope.t('Email was not sent', 'Common.EmailSentError')
                    });
                }
            });
        };

        $scope.cancel = function () {
            dialog.close();
        };


        $scope.attachmentImageSrc = function (attachment) {
            var imageSourceIcon = attachment.imageSourceIcon,
                src = "";
            if (imageSourceIcon) {
                src = imageSourceIcon;
            } else {
                src = '/Components/GetImageThumbnailById?imageId=' + attachment.blobFileStorageId;
            }
            return src;
        };

        $scope.downloadAttachment = function (attachment) {
            var url = "/SelfServicePortal/DownloadFile?blobFileStorageId=" + attachment.blobFileStorageId + "&downloadParent=true";
            window.location = url;
        };


        $timeout(function () {
            var recipentMailsInput = $('#' + $scope.form[0].key);
            recipentMailsInput.on('keyup', function (event) {
                if (event.keyCode === 32) {
                    var inputValue = $(event.target).val();
                    inputValue = inputValue + ' , ';
                    $(event.target).val(inputValue);
                }
            });
        }, 0);
    }]);