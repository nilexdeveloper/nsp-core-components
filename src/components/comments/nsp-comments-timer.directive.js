﻿angular.module('nspCore').directive('nspCommentTimer', ['$http', 'translate', 'appData', 'nspCommentsService', 'nspCoreService', '$interval', '$timeout', function ($http, translate, appData, nspCommentsService, nspCoreService, $interval, $timeout) {
    return {
        name: 'nspCommentTimer',
        scope: {},
        restrict: 'AE',
        templateUrl: '/comments/nsp-comment-timer.html',
        link: function (scope, element, attrs) {

            var startText = translate("Start", "Common.Start"),
                stopText = translate("Stop", "Common.Stop"),
                timerInterval;

            scope.toggleBtnText = startText;
            scope.t = translate;
            scope.running = false;
            scope.timerPattern = /^[0-9]{1,3}[:][0-5]?[0-9]$/;
            scope.timerInput = "";
            scope.seconds = 0;

            scope.toggle = function ($event) {
                if (scope.running) {
                    scope.stopTimer();
                } else {
                    scope.startTimer();
                }
                $event.preventDefault();
            };

            scope.inputChanged = function () {
                var value = scope.timerInput,
                    hours = 0,
                    minutes = 0;
                if (value && scope.timerForm.timerInput.$valid) {
                    hours = parseInt(value.split(':')[0]);
                    minutes = parseInt(value.split(':')[1]);
                    scope.seconds = hours * 3600 + minutes * 60;
                } else if (scope.seconds !== 0) {
                    scope.seconds = 0;
                }
            };

            scope.startTimer = function () {
                scope.running = true;
                scope.toggleBtnText = stopText;
                scope.timerInput = scope.timerInput ? scope.timerInput : "00:01";
                if (timerInterval) {
                    $interval.cancel(timerInterval);
                }
                scope.seconds++;
                timerInterval = $interval(function () {
                    scope.seconds++;
                }, 1000);
                $timeout(function () {
                    scope.$digest();
                }, 0);
            };

            scope.stopTimer = function () {
                scope.running = false;
                scope.toggleBtnText = startText;
                if (timerInterval) {
                    $interval.cancel(timerInterval);
                }
            };

            scope.timerInputBlur = function () {
                if (scope.timerInput && scope.timerForm.timerInput.$valid) {
                    scope.timerInput = nspCoreService.secondsToHoursAndMinutes(scope.seconds);
                    scope.startTimer();
                } else {
                    scope.timerInput = "";
                    scope.stopTimer();
                }
            };

            scope.timerInputFocus = function () {
                scope.stopTimer();
            };

            scope.$watch('seconds', function (newValue, oldValue) {
                if (scope.running) {
                    if (newValue === 0) {
                        scope.timerInput = "";
                    } else if (newValue < 60) {
                        scope.timerInput = "00:01";
                    } else {
                        scope.timerInput = nspCoreService.secondsToHoursAndMinutes(scope.seconds);
                    }
                }
                scope.$emit('commentTimerChanged', newValue);
            });

            scope.$on('destroy', function () {
                $interval.cancel(timerInterval);
            });

            scope.$on('autoTimerStart', function () {
                if (!scope.running) {
                    scope.startTimer();
                }
            });

            scope.$on('timerStop', function () {
                if (scope.running) {
                    scope.stopTimer();
                }
            });

            element.parent().addClass('timer-tool');

        }
    };
}]);

