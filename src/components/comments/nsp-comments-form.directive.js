﻿angular.module('nspCore').directive('nspCommentsForm', ['$http', 'translate', 'appData', 'nspCommentsService', 'nspCoreService', '$filter', '$q', function($http, translate, appData, nspCommentsService, nspCoreService, $filter, $q){
    return {
        name: 'commentsForm',
        scope: {
            options: "=commentsFormOptions"
        },
        restrict: 'AE',
        replace: true,
        templateUrl: '/comments/nsp-comments-form.html',
        link: function(scope, element, attrs) {         

            var defaultOptions = {
                timer: false,
                insertTags: false,
                selectTemplate: false,
                attachmentParamsFlat: true,
                publicMark: true,
                privateMark: true,
                solutionMark: false,
                workaroundMark: false
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = translate;
            scope.formId = scope.settings.id ? scope.settings.id : "commentForm";
            scope.timeSpent = 0;

            var editorOptions = angular.copy(nspCommentsService.editorOptions);

            for (var i = 0; i < editorOptions.tools.length; i++) {
                if ((!scope.settings.insertTags && editorOptions.tools[i].name === "insertTags") ||
                    (!scope.settings.selectTemplate && editorOptions.tools[i].name === "selectTemplate") ||
                    (!scope.settings.timer && editorOptions.tools[i].name === "timer")) {
                    editorOptions.tools.splice(i, 1);
                    i--;
                }
            }

            var commentFieldKey = scope.formId + "Comment",
                schema = {
                    "type": "object",
                    "properties": {}
                },
                form = [{
                    "key": commentFieldKey,
                    "type": "kendo-editor",
                    "title": "Comment",
                    "titleKey": "Common.Comment",
                    "required": true,
                    "kendoOptions": editorOptions,
                    "onChange": function(modelValue) {
                        scope.newComment.comment = modelValue;
                    }
                }],
                model = {};

            schema.properties[scope.formId + 'Comment'] = {
                "type": "string"
            };

            model[commentFieldKey] = "";

            scope.schema = schema;
            scope.form = form;
            scope.model = model;
            scope.newComment = {
                timeSpent: 0,
                comment: "",
                isPublic: appData.person ? appData.person.ispublicComment : false,
                isPrivate: appData.person ? !appData.person.ispublicComment : true,
                isSolution: false,
                isWorkaround: false
            };
            scope.uploadAttachmentsOptions = scope.settings.uploadAttachmentsOptions;

            scope.newCommentMark = function(mark) {
                switch(mark) {
                    case "public":
                        scope.newComment.isPublic = true;
                        scope.newComment.isPrivate = false;
                    break;
                    case "private":
                        scope.newComment.isPublic = false;
                        scope.newComment.isPrivate = true;
                    break;
                    case "solution":
                        scope.newComment.isSolution = !scope.newComment.isSolution;
                        scope.newComment.isWorkaround = false;
                    break;
                    case "workaround":
                        scope.newComment.isSolution = false;
                        scope.newComment.isWorkaround = !scope.newComment.isWorkaround;
                    break;
                }
            };

            /**
             * Form Submit event handler used to validate the form
             */
            scope.onCommentSubmit = function() {
                var form = scope[scope.formId];
                scope.$broadcast('schemaFormValidate');
                if (form.$valid) {
                    scope.submitComment();
                } else {
                    if(window.nspSendCommonNotification){
                        if (window.nspSendCommonNotification.error) {
                            window.nspSendCommonNotification.error({
                              notificationHeader: '',
                              notificationMessage: translate('Comment submit failed.', 'Common.CommentSubmitFailed')
                            });
                        }
                    }  
                }
            };

            /**
             * Submits Comment to back-end
             */
            scope.submitComment = function() {
                var params = {
                    webBrowser: nspCoreService.getBrowser(),
                    comment: scope.newComment
                };
                params = $.extend({}, params, scope.settings.params);

                if (scope.settings.attachmentParamsFlat && params.comment.attachments) {
                    var attachments = [];
                    for (var i = 0; i < params.comment.attachments.length; i++) {
                        attachments.push(params.comment.attachments[i].blobFileStorageId);
                    }
                    params.attachmentIds = attachments;
                }

                // UtilityService.Loader.Transparent.show();
                scope.$broadcast('timerStop');
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: scope.settings.url,
                    dataType: 'json',
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                })
                .success(function(response) {
                    // Reset Form after successful submit
                    scope.model[commentFieldKey] = "";
                    scope.newComment.timeSpent = 0;
                    scope.newComment.comment = "";
                    scope.newComment.attachments = [];
                    scope.$broadcast('fileDropReset');
                    scope.$broadcast('schemaFormRedraw');
                    if (angular.isFunction(scope.settings.onSubmit)) {
                        scope.settings.onSubmit(response);
                    }
                    // UtilityService.Loader.Transparent.hide();
                    deferred.resolve(response);
                })
                .error(function(err, status) {
                    if(window.nspSendCommonNotification){
                        if (window.nspSendCommonNotification.error) {
                          window.nspSendCommonNotification.error({
                            notificationHeader: '',
                            notificationMessage: translate('Comment submit failed.', 'Common.CommentSubmitFailed')
                          });
                        }
                    }                    
                    // UtilityService.Loader.Transparent.hide();
                    deferred.reject(err);
                    throw new Error("Failed to submit comment: " + err);
                }); 
                return deferred.promise;
            };

            scope.$on('commentTimerChanged', function(event, value) {
                if (value > 0 && value < 59) {
                    value = 60;
                }
                scope.newComment.timeSpent = value;
            });

        }
    };
}]);