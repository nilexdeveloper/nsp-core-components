﻿angular.module('nspCore').directive('nspCommentsList', ['$http', 'nspCoreService', '$templateCache', 'appData', 'nspCommentsService', '$filter', 'kendoWindowService', function($http, nspCoreService, $templateCache, appData, nspCommentsService, $filter, kendoWindowService) {
    return {
        name: 'commentsList',
        scope: {
            options: "=commentsListOptions"
        },
        restrict: 'AE',
        templateUrl: '/comments/nsp-comments-list.html',
        link: function(scope, element, attrs) {

            var defaultOptions = {
                publicMark: true,
                privateMark: true,
                solutionMark: false,
                workaroundMark: false
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = nspCoreService.translate;
            scope.commentsList = [];
            scope.allCollapsed = scope.settings.collapsed;

            function getCommentIndex(id) {
                var index = 0;
                for (var i = 0; i < scope.commentsList.length; i++) {
                    if (scope.commentsList[i].id === id) {
                        index = i;
                        break;
                    }
                }
                return index;
            }

            scope.loadComments = function() {
                // UtilityService.Loader.Transparent.show();
                $http({
                        method: 'GET',
                        url: scope.settings.url,
                        dataType: 'json',
                        params: scope.settings.params
                    })
                    .success(function(response) {
                        var data = response;
                        angular.forEach(data.list, function(value, key) {
                            value.comment = nspCoreService.htmlDecode(value.comment);
                            value.collapsed = scope.allCollapsed;
                        });
                        scope.commentsList = data.list;
                        if (scope.options.commentListLength) {
                            scope.options.commentListLength(data.totalCount);
                        }
                        // UtilityService.Loader.Transparent.hide();
                    })
                    .error(function(error, status) {
                        if (window.nspSendCommonNotification.error) {
                            window.nspSendCommonNotification.error({
                                notificationHeader: '',
                                notificationMessage: nspCoreService.translate('Failed to load comments.', 'Common.FailedToLoadComments')
                            });
                        }
                        // UtilityService.Loader.Transparent.hide();
                        throw new Error("Failed to load comments: " + error);
                    });
            };

            scope.timeAgo = function(value, format) {
                return nspCoreService.timeAgoLegacy(value, format);
            };

            scope.formatTimeSpent = function(value) {
                return nspCoreService.secondsToHoursAndMinutes(value);
            };

            scope.changeCommentMark = function(id, mark) {
                var index = getCommentIndex(id);
                switch (mark) {
                    case "public":
                        scope.commentsList[index].isPublic = true;
                        scope.commentsList[index].isPrivate = false;
                        break;
                    case "private":
                        scope.commentsList[index].isPublic = false;
                        scope.commentsList[index].isPrivate = true;
                        break;
                    case "solution":
                        scope.commentsList[index].isSolution = !scope.commentsList[index].isSolution;
                        scope.commentsList[index].isWorkaround = false;
                        break;
                    case "workaround":
                        scope.commentsList[index].isSolution = false;
                        scope.commentsList[index].isWorkaround = !scope.commentsList[index].isWorkaround;
                        break;
                }
                nspCommentsService.commentAction({
                    "id": scope.commentsList[index].id,
                    "isPublic": scope.commentsList[index].isPublic,
                    "isPrivate": scope.commentsList[index].isPrivate,
                    "isSolution": scope.commentsList[index].isSolution,
                    "isWorkaround": scope.commentsList[index].isWorkaround
                }, scope.settings.execUrl);
            };

            scope.likeComment = function(id) {
                var index = getCommentIndex(id);
                if (scope.commentsList[index].isLiked) {
                    return false;
                }
                scope.commentsList[index].likes++;
                scope.commentsList[index].isLiked = true;
                nspCommentsService.commentAction({
                    "id": scope.commentsList[index].id,
                    "isPublic": scope.commentsList[index].isPublic,
                    "isPrivate": scope.commentsList[index].isPrivate,
                    "isSolution": scope.commentsList[index].isSolution,
                    "isWorkaround": scope.commentsList[index].isWorkaround,
                    "isLiked": true
                }, scope.settings.execUrl);
            };

            scope.collapseToggle = function(id) {
                var index = getCommentIndex(id);
                scope.commentsList[index].collapsed = !scope.commentsList[index].collapsed;
            };

            scope.collapseToggleAll = function() {
                scope.allCollapsed = !scope.allCollapsed;
                angular.forEach(scope.commentsList, function(value, key) {
                    value.collapsed = scope.allCollapsed;
                });
            };

            scope.countComments = function(mark) {
                var count = 0;
                switch (mark) {
                    case "solution":
                        count = $filter('filter')(scope.commentsList, {
                            isSolution: true
                        }).length;
                        break;
                    case "workaround":
                        count = $filter('filter')(scope.commentsList, {
                            isWorkaround: true
                        }).length;
                        break;
                    case "attachments":
                        count = $filter('filter')(scope.commentsList, {
                            hasAttachment: true
                        }).length;
                        break;
                    case "all":
                        count = scope.commentsList.length;
                        break;
                }
                return count;
            };

            scope.markFilter = function(filter) {
                scope.activeMarkFilter = scope.activeMarkFilter === filter || !filter ? "" : filter;
            };

            scope.mapArticle = function(item) {
                var kendoWindowOptions = {
                    title: nspCoreService.translate('New Article From Ticket', 'Common.NewArticleFromTicket'),
                    modal: true,
                    width: 800,
                    height: "90%",
                    maxHeight: 460,
                    content: {
                        url: "/Scripts/build/app/views/knowledge-base/article-mapping-form.html"
                    }
                };
                var windowData = {
                    "comment": item,
                    "ticketId": scope.options.params.ticketId,
                    "ticketTypeId": scope.$parent.data.ticketTypeId,
                    "commentId": item.id,
                    "formId": scope.options.formId 
                };

                kendoWindowService.create('ArticleMappingForm', windowData, kendoWindowOptions);
            };

            scope.attachmentImageSrc = function(attachment) {
                var imageSourceIcon = attachment.imageSourceIcon,
                    src = "";
                if (imageSourceIcon) {
                    src = imageSourceIcon;
                } else {
                    src = '/Components/GetImageThumbnailById?imageId=' + attachment.blobFileStorageId;
                }
                return src;
            };

            scope.downloadAttachment = function(attachment) {
                var url = "/Components/DownloadFile?blobFileStorageId=" + attachment.blobFileStorageId + "&downloadParent=true";
                window.location = url;
            };

            scope.sendEmail = function(item) {
                var kendoWindowOptions = {
                    title: nspCoreService.translate('Send Email', 'Common.SendEmail'),
                    modal: true,
                    width: 800,
                    height: "90%",
                    maxHeight: 460,
                    content: {
                        template: $templateCache.get('/comments/nsp-send-email-comment.html')
                    }
                };
                var windowData = {
                    comment: item,
                    additionalData: scope.settings.additionalData,
                    attachments: scope.commentsList
                };

                kendoWindowService.create('commentSendAsEmail', windowData, kendoWindowOptions);
            };

            scope.$on('loadComments', function() {
                // UtilityService.Loader.Transparent.show();
                scope.loadComments();
            });
            scope.loadComments();

        }
    };
}]);

angular.module('nspCore').filter('filterComments', function($filter) {
    return function(items, searchValue, activeMarkFilter) {
        var result = [];
        result = searchValue ? $filter('filter')(items, {
            comment: searchValue
        }) : items;
        switch (activeMarkFilter) {
            case "solution":
                result = $filter('filter')(result, {
                    isSolution: true
                });
                break;
            case "workaround":
                result = $filter('filter')(result, {
                    isWorkaround: true
                });
                break;
            case "attachments":
                result = $filter('filter')(result, {
                    hasAttachment: true
                });
                break;
        }
        return result.slice().reverse();
    };
});