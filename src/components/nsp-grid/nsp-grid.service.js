(function() {
    'use strict';

    angular
        .module('nspCore')
        .factory('nspGridService', nspGridService);

    /* @ngInject */
    function nspGridService(nspCoreService) {
        var factory = {
            gridParameterMap: gridParameterMap,
            searchParameterMap: searchParameterMap
        };
        return factory;

        /**
         * Grid parameter map
         * @param  {object} data       Parameter data
         * @return {object}            Request parameters
         */
        function gridParameterMap(data) {
            var params = data.params,
                result = params;
            result.pageIndex = params.page; // Set page index

            // Set sort criteria
            if (params.sort && params.sort.length) {
                result.sortCriteria = JSON.stringify(params.sort);
            }

            // Set filter criteria
            if (params.filter) {
                result.filterCriteria = angular.toJson(result.filter);
            }

            // Set searchable columns
            result.searchableColumns = getSearchableColumns(data.settings);

            // Set search text
            if (data.settings.searchText) {
                result.searchText = data.settings.searchText;
            }

            return result;
        }

        /**
         * Search parameter map
         * @param  {object} data     Parameter data
         * @return {object}          Request parameters
         */
        function searchParameterMap(data) {
            var result = data.params;

            // Set searchText and filterCriteria from filters
            if (result && result.filter) {
                for (var i = 0; i < result.filter.filters.length; i++) {
                    var currentFilter = result.filter.filters[i];
                    if (currentFilter.field === '') {
                        currentFilter.field = 'grid-search-all';
                        result.searchText = currentFilter.value;
                        break;
                    }
                }

                // Add grid filters
                if (data.gridFilters && data.gridFilters.filters && data.gridFilters.filters.length && data.settings) {
                    data.gridFilters.filters = addFiltersDbType(data.gridFilters.filters, data.settings.gridOptions);
                    for (i = 0; i < data.gridFilters.filters.length; i++) {
                        result.filter.filters.push(data.gridFilters.filters[i]);
                    }
                }
                result.filterCriteria = JSON.stringify(result.filter);
            }
            
            result.searchableColumns = getSearchableColumns(data.settings);
            return result;
        }

        /**
         * Gets searchable columns from NSP grid settings
         * @param  {object} settings NSP Grid full settings
         * @return {array}           List of searchable column field names
         */
        function getSearchableColumns(settings) {
            var result = [];
            // Set searchable columns from gridOptions
            if (settings.gridOptions.columns && settings.gridOptions.columns.length) {
                angular.forEach(settings.gridOptions.columns, function(column, key){
                    if (column.searchable) {
                        result.push(column.field);
                    }
                });
            }
            // Set searchable columns from NSP grid settings.
            // Note: This option overrides other searchableColumns.
            if (settings.searchableColumns) {
                result = settings.searchableColumns;
            }
            return result;
        }

        /**
         * Adds dbType to filters using field model.
         * @param {array}  filtersData Filters
         * @param {object} gridOptions Grid settings
         */
        function addFiltersDbType(filtersData, gridOptions) {
            function iterateForm(data) {
                for (var i = 0; i < data.length; i++) {
                    var current = data[i];
                    if (current.field === '') {
                        continue;
                    }
                    if (current.filters && current.filters.length) {
                        iterateForm(current.filters);
                        continue;
                    }
                    if (nspCoreService.propertyExists(gridOptions, 'dataSource.schema.model.fields')) {
                        if (gridOptions.dataSource.schema.model.fields[current.field]) {
                            data[i].dbType = gridOptions.dataSource.schema.model.fields[current.field].dbType;
                        }
                    }
                }
            }
            iterateForm(filtersData);
            return filtersData;
        }
    }
})();