(function() {
    'use strict';

    angular
        .module('nspCore')
        .directive('nspGrid', nspGrid);

    /* @ngInject */
    function nspGrid($http, $rootScope, $templateCache, $compile, $timeout, nspCoreService, nspGridService) {
        var directive = {
            link: link,
            restrict: 'AE',
            scope: {
                options: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var defaultOptions = {
                template: '/nsp-grid/templates/nsp-grid.template.html',
                gridId: 'nspGrid-' + kendo.guid(),
                gridOptions: {
                    dataSource: {
                        transport: {
                            read: {
                                dataType: "json",
                                type: "POST"
                            },
                            parameterMap: function(data, type) {
                                return nspGridService.gridParameterMap({
                                    params: data,
                                    type: type,
                                    settings: scope.settings
                                });
                            }
                        },
                        schema: {
                            data: 'list',
                            total: 'totalCount'
                        },
                        serverFiltering: true,
                        serverPaging: true,
                        serverSorting: true,
                        pageSize: 20
                    },
                    resizable: true,
                    reorderable: false,
                    filterable: true,
                    columnMenu: {
                        columns: false
                    },
                    sortable: {
                        mode: "single",
                        allowUnsort: true
                    },
                    selectable: "row",
                    scrollable: false,
                    noRecords: {
                        template: "<span>" + nspCoreService.translate('No available data.', 'Common.NoAvailableData') + "</span>"
                    },
                    pageable: {
                        numeric: false,
                        pageSizes: [5, 10, 15, 20],
                        messages: {
                            display: "{0}-{1} " + nspCoreService.translate('of', 'Of') + " {2}",
                            itemsPerPage: nspCoreService.translate('Rows per page', 'Common.RowsPerPage') + ":"
                        }
                    }
                },
                search: true,
                searchText: '',
                searchOptions: {
                    dataSource: {
                        transport: {
                            read: {
                                url: "/CmdbCiView/GetAutoCompleteDataSource",
                                dataType: "json",
                                type: "POST"
                            },
                            parameterMap: function(data, type) {
                                return nspGridService.searchParameterMap({
                                    params: data,
                                    type: type,
                                    settings: scope.settings,
                                    gridFilters: scope.gridWidget.dataSource.filter() ? scope.gridWidget.dataSource.filter() : []
                                });
                            }
                        },
                        schema: {
                            data: "list"
                        },
                        serverFiltering: true
                    },
                    filter: 'contains',
                    template: "<span ng-non-bindable>#=data#</span>",
                    placeholder: nspCoreService.translate('Search...', 'SearchDotDotDot'),
                    minLength: 3,
                    delay: 500
                }
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = nspCoreService.translate;
            scope.refreshGrid = refreshGrid;
            scope.searchGrid = searchGrid;
            scope.reset = reset;
            scope.showReset = false;

            element.addClass('nsp-grid');

            element.html($templateCache.get(scope.settings.template));
            $compile(element.contents())(scope);

            scope.searchWidget.bind('select', onSearchSelect);
            scope.gridWidget.bind('dataBound', onGridDataBound);

            function onSearchSelect(event) {
                var dataItem = scope.searchWidget.dataItem(event.item.index());
                scope.settings.searchText = dataItem;
                searchGrid();
            }

            function searchGrid() {
                scope.gridWidget.dataSource.page(1);
            }

            function refreshGrid() {
                scope.gridWidget.dataSource.read().then(function() {
                    scope.gridWidget.refresh();
                });                
            }

            function reset() {
                scope.searchWidget.value('');
                scope.settings.searchText = '';
                scope.gridWidget.dataSource.query({
                    sort: getFixedSorters(),
                    filter: getFixedFilters(),
                    pageSize: scope.settings.gridOptions.dataSource.pageSize,
                    page: 1,
                    skip: 0
                });
                if (angular.isFunction(scope.settings.onReset)) {
                    scope.settings.onReset(scope);
                }
            }

            function onGridDataBound(event) {
                $timeout(function() {
                    scope.showReset = isResetVisible();
                }, 0);
            }

            function getFixedFilters() {
                var result = [],
                    gridFilters = scope.gridWidget.dataSource.filter() ? scope.gridWidget.dataSource.filter() : [];

                if (gridFilters.filters) {
                    angular.forEach(gridFilters.filters, function(filter, key) {
                        if (filter.fixed) {
                            result.push(filter);
                        }
                    });
                }
                return result;
            }

            function getFixedSorters() {
                var result = [],
                    gridSorters = scope.gridWidget.dataSource.sort() ? scope.gridWidget.dataSource.sort() : [];

                angular.forEach(gridSorters, function(sorter, key) {
                    if (sorter.fixed) {
                        result.push(sorter);
                    }
                });
                return result;
            }

            function isResetVisible() {
                var result = false,
                    gridFilters = scope.gridWidget.dataSource.filter() ? scope.gridWidget.dataSource.filter() : [],
                    gridSorters = scope.gridWidget.dataSource.sort() ? scope.gridWidget.dataSource.sort() : [];

                angular.forEach(gridSorters, function(filter, key) {
                    if (!filter.fixed) {
                        result = true;
                    }
                });

                if (gridFilters.filters) {
                    angular.forEach(gridFilters.filters, function(filter, key) {
                        if (!filter.fixed) {
                            result = true;
                        }
                    });
                }

                if (scope.settings.searchText.length) {
                    result = true;
                }
                return result;
            }
        }
    }
})();