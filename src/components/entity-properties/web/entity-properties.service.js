(function() {
    'use strict';

    angular
        .module('nspCore')
        .service('nspCoreEntityPropertyService', nspCoreEntityPropertyService);

    nspCoreEntityPropertyService.$inject = [];

    /* @ngInject */
    function nspCoreEntityPropertyService() {

        var factory = {};

        var templates = {
            'NSPCheckBoxProperty': '/entity-properties/web/templates/check-box.html',
            'NSPCheckBoxesProperty': '/entity-properties/web/templates/check-boxes.html',
            'NSPHelpProperty': '/entity-properties/web/templates/textarea.html',
            'NSPKendoAutoCompleteProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoComboBoxProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoDatePickerProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoDateTimePickerProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoDropDownListProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoEditorProperty': '/entity-properties/web/templates/textarea.html',
            'NSPKendoNumericTextBoxProperty': '/entity-properties/web/templates/text.html',
            'NSPKendoTimePickerProperty': '/entity-properties/web/templates/text.html',
            'NSPRadiosProperty': '/entity-properties/web/templates/radio-button.html',
            'NSPTextProperty': '/entity-properties/web/templates/text.html',
            'string': '/entity-properties/web/templates/text.html',
            'int': '/entity-properties/web/templates/text.html',
            'boolean': '/entity-properties/web/templates/check-box.html',
            'textarea': '/entity-properties/web/templates/textarea.html',
            'image': '/entity-properties/web/templates/image.html',
            'date': '/entity-properties/web/templates/text.html',
            'time': '/entity-properties/web/templates/text.html',
            'datetime': '/entity-properties/web/templates/text.html',
            'popup': '/entity-properties/web/templates/popup.html',
            'info': '/entity-properties/web/templates/textarea.html'
        },
        popupTemplates = {
            'product': '/entity-properties/web/entity-property-product-popup.html',
            'vendor': '/entity-properties/web/entity-property-vendor-popup.html',
            'location': '/entity-properties/web/entity-property-location-popup.html',
            'usedby': '/entity-properties/web/entity-property-user-popup.html',
            'managedby': '/entity-properties/web/entity-property-user-popup.html',
            'requester': '/entity-properties/web/entity-property-user-popup.html',
            'approver': '/entity-properties/web/entity-property-user-popup.html'
        },
        popupUrl = {
            'usedby': '/UserManagement/GetPerson',
            'managedby': '/UserManagement/GetPerson',
            'requester': '/UserManagement/GetPerson',
            'approver': '/UserManagement/GetPerson',
            'location': '/UserManagement/GetAddress',
            'vendor': '/CmdbVendor/GetVendor',
            'product': '/CmdbProduct/GetProduct'
        },
        formats = {
            'NSPKendoDatePickerProperty': 'd',
            'NSPKendoTimePickerProperty': 't',
            'NSPKendoDateTimePickerProperty': 'g',
            'date': 'd',
            'time': 't',
            'datetime': 'g'
        };
        
        factory = {
            templates: templates,
            popupTemplates: popupTemplates,
            popupUrl: popupUrl,
            formats: formats
        };

        return factory;
    }
})();