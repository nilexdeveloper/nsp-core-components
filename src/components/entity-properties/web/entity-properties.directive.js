(function() {
    'use strict';

    angular
        .module('nspCore')
        .directive('nspCoreEntityProperty', nspCoreEntityProperty);

    nspCoreEntityProperty.$inject = ['$http', '$templateCache', '$timeout', '$compile', '$sce', 'nspCoreService', 'nspCoreEntityPropertyService', 'kendoWindowService'];

    /* @ngInject */
    function nspCoreEntityProperty($http, $templateCache, $timeout, $compile, $sce, nspCoreService, nspCoreEntityPropertyService, kendoWindowService) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                options: "=options"
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var templateUrl = nspCoreEntityPropertyService.templates[scope.options.nspType],
                defaultOptions = {};

            scope.t = nspCoreService.translate;

            switch(scope.options.nspType) {
                case 'NSPTextProperty':
                case 'NSPKendoAutoCompleteProperty':
                case 'NSPKendoDropDownListProperty':
                case 'NSPKendoComboBoxProperty':
                    scope.propertyType = 'text';
                    if (scope.options.viewType && scope.options.viewType === 'popup') {
                        scope.propertyType = 'popup';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.viewType];
                    }
                    break;
                case 'NSPKendoTimePickerProperty':
                case 'NSPKendoDatePickerProperty':
                case 'NSPKendoDateTimePickerProperty':
                    scope.propertyType = 'date-time';
                    var parsedDate = kendo.parseDate(scope.options.value);
                    scope.options.value = kendo.toString(parsedDate, nspCoreEntityPropertyService.formats[scope.options.nspType]);
                    break;
                case 'NSPRadiosProperty':
                    scope.options.value = scope.options.type === 'string' ? JSON.parse(scope.options.value) : scope.options.value;
                    scope.propertyType = 'radio-button';
                    break;
                case 'NSPCheckBoxesProperty':
                    scope.options.value = scope.options.type === 'string' ? JSON.parse(scope.options.value) : scope.options.value;
                    scope.propertyType = 'check-boxes';
                    break;
                case 'NSPCheckBoxProperty':
                    scope.propertyType = 'check-box';
                    scope.options.value = scope.options.value === true || scope.options.value.toUpperCase() === 'TRUE' ? true : false;
                    break;
                case 'NSPKendoEditorProperty':
                    scope.propertyType = 'textarea';
                    break;
                case 'NSPKendoNumericTextBoxProperty':
                    scope.propertyType = 'number';
                    break;
                case 'NSPHelpProperty':
                    scope.propertyType = 'help';
                    break;
                default:
                    if (scope.options.type === 'string' || scope.options.type === 'int') {
                        scope.propertyType = 'text';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'date' || scope.options.type === 'time' || scope.options.type === 'datetime') {
                        scope.propertyType = 'date-time';
                        var parseDate = kendo.parseDate(scope.options.value);
                        scope.options.value = kendo.toString(parseDate, nspCoreEntityPropertyService.formats[scope.options.type]);
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'boolean') {
                        scope.propertyType = 'check-box';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'image') {
                        scope.propertyType = 'image';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else if (scope.options.type === 'info' || scope.options.type === 'textarea') {
                        scope.propertyType = 'textarea';
                        templateUrl = nspCoreEntityPropertyService.templates[scope.options.type];
                    } else {
                        scope.propertyType = 'text';
                        templateUrl = nspCoreEntityPropertyService.templates.NSPTextProperty;
                    }
            }

            scope.trustHtml = function(string){
                return $sce.trustAsHtml(string);
            };

            scope.openPopup = function() {
                if (scope.options.id) {
                    var title;
                    if (scope.options.entityType === 'usedby' || scope.options.entityType === 'managedby' || scope.options.entityType === 'requester' || scope.options.entityType === 'approver') {
                        title = nspCoreService.translate('User', 'Common.User');
                    }

                    var kendoWindowOptions = {
                        title: title ? title : nspCoreService.translate(scope.options.label, scope.options.labelKey),
                        modal: true,
                        width: 830,
                        height: 330,
                        content: {
                            url: false,
                            template: $templateCache.get(nspCoreEntityPropertyService.popupTemplates[scope.options.entityType])
                        }
                    };

                    var windowData = {
                        data: scope.options
                    };

                    kendoWindowService.create('entityPropertyPopup', windowData, kendoWindowOptions);
                }    
            };

            scope.options = angular.merge({}, defaultOptions, scope.options);

            element.html($templateCache.get(templateUrl));
            $compile(element.contents())(scope);
            
        }
    }
})();