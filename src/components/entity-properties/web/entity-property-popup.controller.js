(function() {
    'use strict';

    angular
        .module('nspCore')
        .controller('EntityPropertyPopupController', EntityPropertyPopupController);

    EntityPropertyPopupController.$inject = ['$scope', '$http', '$timeout', '$sce', 'nspCoreService', 'nspCoreEntityPropertyService', 'kendoWindowService'];

    /* @ngInject */
    function EntityPropertyPopupController($scope, $http, $timeout, $sce, nspCoreService, nspCoreEntityPropertyService, kendoWindowService) {
        var vm = this,
            dialogId = 'entityPropertyPopup';

        $scope.dialogData = kendoWindowService.read(dialogId);
        $scope.t = nspCoreService.translate;
        $('#' + dialogId).parent().addClass('entity-property-popup');

        var params = {
            id: $scope.dialogData.data.id
        };

        $scope.trustHtml = function(string){
            return $sce.trustAsHtml(string);
        };

        $scope.buildMap = function(lat, long) {
            var mapCanvas = document.getElementById("map-canvas");
            if (mapCanvas) {
                var mapOptions = {
                    center: new google.maps.LatLng(lat, long),
                    zoom: 10,
                    mapTypeControlOptions: {
                        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                    }
                };
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({
                    position: mapOptions.center,
                    map: map
                });
            }
        };

        $http({
            method: 'GET',
            url: nspCoreEntityPropertyService.popupUrl[$scope.dialogData.data.entityType],
            params: params
        })
        .success(function(data) {
            $scope.data = data;

            if ($scope.dialogData.data.entityType === 'product') {
                $scope.qrcode = $scope.t('Name', 'Common.Name') + ":" + $scope.data.name + $scope.t('Product Type', 'Common.ProductType') + ":" + $scope.data.ciTypeName + $scope.t('Manufacturer', 'Common.Manufacturer') + ":" + $scope.data.manufacturerName;
            } else {
                if ($scope.data.location) {
                    var longitude = $scope.data.location.longitude;
                    var latitude = $scope.data.location.latitude;
                      
                    $timeout(function(){
                        $scope.buildMap(latitude, longitude);
                    }, 0);
                }
            }
        })
        .error(function(data) {
        });
    }
})();