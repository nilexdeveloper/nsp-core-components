angular.module('nspCore').directive('nspCoreTextPanePopupImages', [function() {
    return {
        restrict: 'AE',
        link: function(scope, element, attrs) {
            element.on('click', 'img', function() {
                var $this = $(this),
                    width = $this.width(),
                    height = $this.height(),
                    naturalWidth = this.naturalWidth,
                    naturalHeight = this.naturalHeight;
                if (width < naturalWidth || height < naturalHeight) {
                    var dialogElement = document.createElement("div");
                    var dialog = $(dialogElement).kendoWindow({
                        modal: true,
                        resizable: false,
                        width: "90%",
                        height: "80%",
                        deactivate: function () {
                            this.destroy();
                        }
                    }).data('kendoWindow');
                    dialog.content('<div style="text-align: center"><img src="' + $this.prop('src') + '" ></div>');
                    $(dialogElement).parent().addClass('kendo-window');
                    dialog.center();
                    dialog.open();
                }
            });
        }
    };
}]);