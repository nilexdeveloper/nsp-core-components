angular.module('nspCore').directive('kendoWindowContent', ['$http', 'kendoWindowService', function($http, kendoWindowService) {
    return {
        restrict: 'AE',
        scope: true,
        link: function(scope, element, attrs) {
            var dialogId = element[0].parentElement.id;
            scope.dialogId = dialogId;
            scope.dialogData = kendoWindowService.read(dialogId);
        }
    };
}]);