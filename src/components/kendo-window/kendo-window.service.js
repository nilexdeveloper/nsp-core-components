angular.module('nspCore').factory('kendoWindowService', ['$timeout', '$compile', '$templateCache', function($timeout, $compile, $templateCache) {
    var factory = {
        dialogs: {}
    };

    var defaultOptions = {
        title: "Window",
        modal: true,
        width: 400,
        openWindowManually: false,
        content: {
            url: false,
            template: $templateCache.get('/kendo-window/delete-window.html')
        },
        resizable: false,
        deactivate: function () {
            var dialogId = this.element[0].id,
                dialogScopeElem = $('#' + dialogId + ' > .ng-scope');

            if (factory.dialogs[dialogId].onWindowClose) {
                factory.dialogs[dialogId].onWindowClose(factory.dialogs[dialogId]);
            }
            if (dialogScopeElem.length) {
                dialogScopeElem.scope().$destroy();
            }
            factory.remove(dialogId);
        },
        refresh: function() {
            var that = this,
                content = that.element;
            $timeout(function() {
                var contentScope = content.scope();
                $compile(content.contents())(contentScope);
                contentScope.$digest();

                if (!that.options.openWindowManually) {
                    that.center();
                    that.open();
                }
            }, 0);
        }
    };

    factory.create = function(id, data, opts, callback) {

        // Prevent duplicate kendo window creation
        if ($('#' + id).length) {
            return false;
        }

        if (opts.content && (opts.content.url || opts.content.template)) {
            delete defaultOptions.template;
        }

        var dialog = document.createElement("div"),
            options = $.extend({}, defaultOptions, opts);
            
        dialog.setAttribute("id", id);
        $(dialog).kendoWindow(options);
        factory.dialogs[id] = data;
        if (callback) {
            callback();
        }
        $("#" + id).parent().addClass("kendo-window");
    };

    factory.read = function(id) {
        return factory.dialogs[id];
    };

    factory.remove = function(id) {
        var dialog = $('#' + id).data('kendoWindow');
        dialog.destroy();
        if (factory.dialogs[id].onWindowRemove) {
            factory.dialogs[id].onWindowRemove(factory.dialogs[id]);
        }
        delete factory.dialogs[id];
    };

    return factory;
}]);