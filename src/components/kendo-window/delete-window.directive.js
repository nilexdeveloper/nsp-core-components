angular.module('nspCore').directive('deleteWindow', ['$http', 'UtilityService', 'kendoWindowService', 'translate', function($http, UtilityService, kendoWindowService, translate) {
    return {
        restrict: 'AE',
        scope: true,
        link: function(scope, element, attrs) {

            var dialogId = element[0].parentElement.id,
                dialog = $('#' + dialogId).data('kendoWindow');
            dialogData = kendoWindowService.read(dialogId);
            var successNotification,
                errorNotification;
            scope.data = dialogData;
            if (scope.data.isRemove) {
                scope.message = scope.data.message ? scope.data.message : translate('Are you sure you want to remove?', 'Common.RemovalQuestion');
                successNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('Removed Successfully', 'Common.SuccessfulRemoval')
                };
                errorNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('This item cannot be removed', 'Common.FailedRemoval')
                };
            } else {
                scope.message = scope.data.message ? scope.data.message : translate('Are you sure you want to delete?', 'CiStates.DelMess');
                // generic success notification
                successNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('Deleted Successfully', 'DeletedSuccessfully')
                };
                // generic error notification
                errorNotification = {
                    notificationHeader: '',
                    notificationMessage: translate('This item cannot be deleted.', 'Common.ThisItemCannotBeDeleted')
                };
            }
            scope.confirmButtonText = dialogData.confirmButtonText ? dialogData.confirmButtonText : translate('Yes', 'Yes');
            scope.cancelButtonText = dialogData.cancelButtonText ? dialogData.cancelButtonText : translate('No', 'No');

            // override success notification options if they are customized
            if (dialogData.successNotification) {
                angular.extend(successNotification, dialogData.successNotification);
            }

            // override error notification options if they are customized
            if (dialogData.errorNotification) {
                angular.extend(errorNotification, dialogData.errorNotification);
            }

            scope.delete = function() {

                UtilityService.Loader.Transparent.show();
                if (scope.data.deleteUrl) {
                    $http({
                            method: 'POST',
                            url: scope.data.deleteUrl,
                            data: scope.data.params ? $.param(scope.data.params) : null,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                        .success(function(response) {
                            UtilityService.Loader.Transparent.hide(0);
                            if (response == 'True' || response == '-13' || response == 'true' || response === true) {
                                if (scope.data.onSuccess) {
                                    scope.data.onSuccess(response);
                                }

                                window.nspSendCommonNotification.success(successNotification);
                            }
                            if (response == 'False' || response == '-14' || response == '-15' || response == 'false' || response === false) {
                                if (scope.data.onFailed) {
                                    scope.data.onFailed(response);
                                }

                                window.nspSendCommonNotification.error(errorNotification);
                            }
                            dialog.close();
                        })
                        .error(function(response) {
                            UtilityService.Loader.Transparent.hide(0);
                        });
                } else {
                    if (angular.isFunction(scope.data.callback)) {
                        scope.data.callback();
                        dialog.close();
                    }
                    UtilityService.Loader.Transparent.hide(0);
                }
            };

            scope.cancel = function() {
                dialog.close();
            };
        }
    };
}]);