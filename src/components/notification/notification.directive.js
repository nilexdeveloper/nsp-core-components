(function() {
    'use strict';

    angular
        .module('nspCore')
        .directive('nspNotification', nspNotification);

    /* @ngInject */
    function nspNotification($templateCache, nspCoreService) {
        var directive = {
            link: link,
            restrict: 'AE',
            templateUrl: '/notification/notification.html',
            scope: {
                options: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            var defaultOptions = {
                kendoOptions: {
                    position: {
                        pinned: true,
                        top: 20,
                        right: 20,
                        width: 200
                    },
                    stacking: 'down',
                    hideOnClick: false,
                    appendTo: '.notification-container',
                    autoHideAfter: 7000,
                    button: true,
                    templates: [
                        {
                            type: "success",
                            template: $templateCache.get('/notification/templates/default.html')
                        },
                        {
                            type: "warning",
                            template: $templateCache.get('/notification/templates/default.html')
                        },
                        {
                            type: "info",
                            template: $templateCache.get('/notification/templates/default.html')
                        },
                        {
                            type: "error",
                            template: $templateCache.get('/notification/templates/default.html')
                        }
                    ]
                }
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = nspCoreService.translate;
            scope.close = close;

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('show', onShow);
            });

            scope.$on('notify', function(event, data) {
                scope.widget.show(data, data.type);
            });

            function onShow(event) {
                event.element.addClass('nsp-notification');
            }

            function close(event) {
                event.target.closest('.k-notification').remove();
            }

        }
    }
})();