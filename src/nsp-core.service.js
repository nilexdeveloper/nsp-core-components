(function() {
    'use strict';

    angular
        .module('nspCore')
        .factory('nspCoreService', nspCoreService);

    nspCoreService.$inject = ['$http', '$sce'];

    /* @ngInject */
    function nspCoreService($http, $sce) {

        var factory = {
            propertyExists: propertyExists,
            translate: translate,
            timeAgo: timeAgo,
            timeAgoLegacy: timeAgoLegacy,
            getBrowser: getBrowser,
            htmlEncode: htmlEncode,
            htmlDecode: htmlDecode,
            secondsToHoursAndMinutes: secondsToHoursAndMinutes,
            getParameterByName: getParameterByName
        };

        var tempDocument = null;

        return factory;

        function propertyExists(obj, key) {
            return key.split(".").every(function(x) {
                if (typeof obj != "object" || obj === null || !(x in obj))
                    return false;
                obj = obj[x];
                return true;
            });
        }

        function translate(text, key) {
            var result = key;
            if (NSP && NSP.Translate && NSP.Translate.t(key)) {
                result = NSP.Translate.t(key);
            }
            return result;
        }

        /**
         * Moment.js time ago formatter
         * @param  {string} value Date value in ISO standard
         * @return {string} Time ago formatted date
         */
        function timeAgo(value) {
            if (moment && value) {
                return moment(value).fromNow();
            }
        }

        /**
         * Legacy time ago formatter
         * @param  {string} dateString Date value in iso standard
         * @param  {string} pattern    Date format
         * @return {string} Time ago formatted date
         */
        function timeAgoLegacy(dateString, pattern) {
            if (dateString === null) {
                return '';
            } else {

                var date = new Date(dateString);

                if (date < new Date()) {
                    var seconds = Math.floor((new Date() - date) / 1000);
                    var interval = Math.floor(seconds / 31536000);
                    interval = Math.floor(seconds / 2592000);
                    interval = Math.floor(seconds / 86400);
                    interval = Math.floor(seconds / 3600);

                    interval = Math.floor(seconds / 60);

                    if (seconds <= 59) {
                        return translate("Just now", "JustNow");
                    } else if (seconds > 59 && seconds <= 119) {
                        return interval + " " + factory.translate("minute ago", "MinuteAgo");
                    } else if (seconds > 119 && seconds <= 3599) {
                        return interval + " " + factory.translate("minutes ago", "MinutesAgo");
                    } else {
                        return kendo.toString(date, pattern);
                    }
                } else {
                    return kendo.toString(date, pattern);
                }
            }
        }

        function getBrowser() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                if (tem !== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) !== null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        }

        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value) {
            return $sce.trustAsHtml($('<div/>').html(value).text());
        }

        function secondsToHoursAndMinutes(seconds) {
            var result = "",
                hours = seconds ? Math.floor(seconds / 3600) : 0,
                minutes = seconds ? Math.floor((seconds - (hours * 3600)) / 60) : 0;
            if (hours.toString().length === 1) {
                hours = '0' + hours;
            }
            if (minutes.toString().length === 1) {
                minutes = '0' + minutes;
            }
            result = hours + ":" + minutes;
            return result;
        }

        function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

    }
})();